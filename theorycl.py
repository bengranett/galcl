import sys,os
import pylab
import numpy as N
import bins
import ISWCalc
import cPickle as pickle
import time
#import theorynl, theoryom

dict = {}

def theory(l=None,dndz=None,dndz2=None,noise=0,lmax = 1e4, dononlinear=1, 
            cache='cache/theorycl_%s.pickle',label='ciao', compute_kernel=False, **cosmoparams):
    """ """
    global dict

    if l is None:
        l = N.arange(2,lmax)
    else:
        lmax = l[-1]+1

    assert(dndz is not None)

    z,d = dndz
    cambz = N.sum(z*d)/N.sum(d)
    if dndz2 is not None:
        z2,d2 = dndz2
        cambz = N.sum(z*d*d2)/N.sum(d*d2)

    print "camb z=",cambz

    tag = '%s_z%i_%i_l%i'%(label,cambz*10000,dononlinear,lmax)
        
    if not cache==None:
        cache = cache%tag
        if dict.has_key(cache): return dict[cache]
        if os.path.exists(cache):
            print "> loading Cls from cache",cache
            a = pickle.load(file(cache))
            dict[cache] = a
            return a


    C = ISWCalc.ISWCalculator(dndz,cambz=cambz,dononlinear=dononlinear,params=cosmoparams)

    print "computing Cl"
    cl = C.GalaxyCl_flat(l, dndz2=dndz2)
    print "...done"

    lvec = l.copy()
    
    if compute_kernel:
        phi = C.getPhi()
        phi2 = phi
        if not dndz2==None:
            phi2 = C.getPhi(dNdz=dndz2)
            
        out = []
        dlnk = C.lk[1:]-C.lk[:-1]  # this is natural log

        k = (C.k[1:]+C.k[:-1])/2.
        for l in lvec:
            r = l*1./k
            f = 1./l*phi(r)*phi2(r)*r**4*k*dlnk
            out.append(f)
        kernel = (N.log10(k),N.array(out))

    if not cache==None:
        pickle.dump((lvec,cl),file(cache,'w'))
    return lvec,cl


def kernel(lvec,dndz):
    C = ISWCalc.ISWCalculator(dndz,cambz=cambz,dononlinear=dononlinear)
    phi = C.getPhi()
    out = []
    dlnk = C.lnk[1:]-C.lnk[:-1]
    k = (C.k[1:]+C.k[:-1])/2.
    for l in lvec:
        r = l*1./k
        f = 1./l*phi(r)**2*r**4*k*dlnk
        out.append(f)
    return N.log10(k),f


def computecl(sample1,sample2=None,savefile='data/cl/cl_%s.npy'):
    """ Compute galaxy Cl for sample1 x sample2

    Inputs
    sample1 - id
    sample2 - id
    savefile - cache to save results

    Outputs
    l,Cl
    """
    if sample2 is None:
        sample2 = sample1

    tag = "%i-%i"%(sample1,sample2)
    savefile = savefile%tag

    if os.path.exists(savefile):
        return N.load(savefile)

    zdists = N.load(params.zdist_path)
    zcent = zdists[0]
    zdist = zdists[1:]

    dndz1 = zdist[sample1]
    dndz2 = zdist[sample2]

    cl = theory(dndz=dndz, dndz2=dndz2, lmax=10000, cache=None)
    N.save(savefile, cl)

    return cl

def test():
    zdists = N.load('data/zdist/zdist.npy')
    zcent,dndz1,dndz2 = zdists[:3]


    pylab.plot(zcent,dndz1)
    pylab.plot(zcent,dndz2)
    
    pylab.figure()

    a = theory(dndz=(zcent,dndz1),lmax=1000, cache='cache/theorytest_%s.pickle')
    b = theory(dndz=(zcent,dndz2),lmax=1000, cache='cache/theorytest3_%s.pickle')
    c = theory(dndz=(zcent,dndz1),dndz2=(zcent,dndz2),lmax=1000, cache='cache/theorytest4_%s.pickle')



    pylab.subplot(211)
    pylab.plot(a[0], a[1])
    pylab.plot(b[0], b[1])
    pylab.plot(c[0], c[1])

    pylab.xlim(50,1000)
    pylab.subplot(212)
    pylab.plot(a[0], 1e4*a[0]*a[1])
    pylab.plot(b[0], 1e4*b[0]*b[1])
    pylab.plot(c[0], 1e4*c[0]*c[1],dashes=[4,1])


    pylab.xlim(50,1000)

    
    pylab.show()


if __name__=="__main__":
    test()
    #computecl(0,'cl_%s_225.pickle','ciao')
