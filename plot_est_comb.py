import sys
import numpy as N
import pylab
import cPickle as pickle

import bins

def sqrt(m, rcond=1e-15):
    """ """
    u,s,v = N.linalg.svd(m)
    print s.min(),s.max()


    thresh = N.abs(s).max()*rcond

    s[N.abs(s)<thresh] = 0

    r = N.zeros(len(s))
    r[s>0] = 1./s[s>0]

    assert(N.all(r>=0))
    r = N.sqrt(r)

    out = N.dot(N.dot(u,N.diag(r)),v)

    return out

def sum_twobytwo(m):
    """ """
    sx,sy = m.shape
    o = 0.
    for i in range(2):
        for j in range(2):
            r = m[slice(i,sx-1+i,2),slice(j,sy-1+j,2)]
            o+=r

    return o

def main(paths):
    """ """
    fish_comb = 0.
    est_comb = 0.
    noise_comb = 0.
    for path in paths:
        print path
        bx,clb,est,noiseb,bands,fishbands,fish,cl0 = pickle.load(file(path))
        fish_comb = fish_comb+fish
        est_comb = est_comb+est
        noise_comb = noise_comb+noiseb

    fish = fish_comb
    est = est_comb
    noiseb = noise_comb

    fx = (fishbands[1:]+fishbands[:-1])/2.

    imax = len(est)
    #A = sqrt(fish)
    A = N.eye(len(fish))

    window = N.dot(A,fish)
    norm1 = N.sum(window,axis=1)

    A = N.transpose(A/norm1)

    window = N.dot(A,fish)


    print N.sum(window[0,:])

    #print "> norm check!", N.allclose(N.sum(window,axis=1),1)


    pylab.figure(1,figsize=(4,8))
    subploti=1
    for i in range(5):
        pylab.subplot(5,1,subploti)
        y = window[i*10]
        #print "norm check",N.sum(y)
        pylab.semilogy(fx,y**2,".-")
        subploti+=1
    pylab.legend()

    cov = N.dot(A,N.dot(fish,A.T))

    pylab.figure()

    s = cov.diagonal()**.5
    pylab.imshow(cov/N.outer(s,s),interpolation='None')
    pylab.colorbar()
    pylab.title(path)
    
    x = (bands[1:]+bands[:-1])/2.

    var = N.diag(cov)
    var = var[:imax]

    sig = N.sqrt(var)

    est -= noiseb
    est = N.dot(A,est)

    Cinv = N.linalg.inv(cov)



    l = N.arange(len(cl0))
    clb = bins.binit(l,l*cl0,bands)
    bx = (bands[1:]+bands[:-1])/2.


    fclb = N.dot(window,clb)


    Cx = N.dot(Cinv,fclb)
    bias = N.dot(est,Cx)/ N.dot(fclb,Cx)
    bias_var = 1./N.dot(fclb,Cx)
    est /= bias

    resid = est-fclb
    chi2 = N.dot(resid,N.dot(Cinv,resid))
    print ">",path
    print "   bias: %3.2f +- %3.2f"%(bias,bias_var**.5)
    print "   chi2: %g (dof %i)"%(chi2,len(est))

    pylab.figure(0)
    pylab.plot(x,est,label="%s chi2=%3.2f"%(path[12:20],chi2))
    pylab.plot(x,fclb)
    pylab.fill_between(x,fclb-sig,fclb+sig,edgecolor='None',facecolor='lightblue')

    #pylab.plot(x,clb,dashes=(4,1))


    pylab.legend()




if __name__=="__main__":


    main(sys.argv[1:])
    pylab.show()
