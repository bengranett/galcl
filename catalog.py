import os
import numpy as N
import params
import Mask


def load():
    """ """
    if os.path.exists(params.catalog_path_cache):
        return N.load(params.catalog_path_cache)

    cat = []
    for field in params.field_names:

        field_mask_pack = params.fields[field]['field_mask']
        hole_mask_pack = params.fields[field]['hole_mask']
        M = Mask.Mask()
        M.add_mask_file(field_mask_pack[0],holes=False,format=field_mask_pack[1])
        M.add_mask_file(hole_mask_pack[0],holes=True,format=hole_mask_pack[1])

        path = params.catalog_path%field
        print path

        data = []
        for line in file(path):
            w = line.split()
            
            stargal = w[4]
            if not stargal == "0": continue
            chi2 = float(w[10])
            if chi2>100 : continue

            photz = float(w[7])
            if photz < 0.1: continue
            if photz > 2: continue

            u,g,r,i,iy,z = [float(v) for v in w[25:31]]
            if i < 0:
                i = iy

            if i > 22.5: continue
            if i < 17.5: continue

            ra = float(w[1])
            dec = float(w[2])
            

            data.append( (ra,dec,photz,u,g,r,i,z) )

        data = N.array(data)
    
        n1 = len(data)
    
        ra,dec = data.T[:2]

        good = M.check_inside(ra,dec)

        n2 = N.sum(good)
        print "lost to the mask",n1-n2,n2*1./n1

        data = data[good]
        cat.append(data)
    cat = N.vstack(cat)

    N.save(params.catalog_path_cache, cat)

    return cat


if __name__=="__main__":
    load()
