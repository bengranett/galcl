"""spherical geometry utilities"""


import numpy as N

# degrees to radian conversions
c = N.pi/180
ic = 180/N.pi

fullsky = 4*N.pi*(180/N.pi)**2
steradian = 4*N.pi
steradian_to_degree = (180/N.pi)**2

def lonlat2xyz(lon,lat,r=1):
    """ """
    x = r*N.cos(lon*c)*N.cos(lat*c)
    y = r*N.sin(lon*c)*N.cos(lat*c)
    z = r*N.sin(lat*c)
    return x,y,z

def xyz2lonlat(x,y,z,getr=True):
    """ """
    if getr:
        r = N.sqrt(x*x+y*y+z*z)
    else:
        r = N.ones(x.shape)
    lat = N.arcsin(z/r)*ic
    lon = N.arctan2(y,x)*ic
    if getr:
        return lon,lat,r
    return lon,lat

def rotate_xyz(x,y,z,angles=None,inverse=False):
    """ Rotate a set of vectors pointing in the direction x,y,z

    angles is a list of longitude and latitude angles to rotate by.
    First the longitude rotation is applied (about z axis), then the
    latitude angle (about y axis).
    """
    if angles==None:
        return x,y,z

    xyz = N.array([x,y,z])
    m = N.eye(3)
    for dlon,dlat in angles:
        dlon*=c
        dlat*=c
        m1 = N.array([[N.cos(dlon),-N.sin(dlon),0],
                      [N.sin(dlon), N.cos(dlon),0],
                      [0,0,1]])

        m2 = N.array([[N.cos(dlat),0,-N.sin(dlat)],
                      [0,1,0],
                      [N.sin(dlat), 0, N.cos(dlat)]])

        m = N.dot(N.dot(m2,m1),m)

    if inverse:
        m = N.linalg.inv(m)
        
    xyz2 = N.dot(m,xyz)
    return xyz2

def rotate_lonlat(lon,lat,angles=[(0,0)]):
    """ Rotate a set of longitude and latitude coordinate pairs.
    """
    xyz = N.array(lonlat2xyz(lon,lat))
    xyz2 = rotate_xyz(*xyz,angles=angles)
    return xyz2lonlat(*xyz2,getr=False)


def distance(a, b):
    """compute angular separation on the sphere"""
    lon1,lat1 = a
    lon2,lat2 = b
    dlon = (lon1-lon2)/2.*c
    dlat = (lat1-lat2)/2.*c
    x = N.sin(dlat)**2+N.cos(lat1*c)*N.cos(lat2*c)*N.sin(dlon)**2
    return ic*2*N.arcsin(N.sqrt(x))


def sample_cap(lon=0., lat=0., radius=90, n=1000):
    """ Draw uniform randoms on a spherical cap.

    Inputs 
    lon - longitude of center (Degree)
    lat - latitude of center (Degree)
    radius - radius (Degree)
    n - int number of samples to draw

    Outputs
    lon - longitude (Degree)
    lat - latitude (Degree)
    area - Area of spherical cap (Steradians)

    """
    r = radius*N.pi/180

    area = 2*N.pi*(1-N.cos(r))

    # generate a cap
    z = N.random.uniform(N.cos(r),1,n)
    theta = N.arcsin(z) * 180/N.pi
    phi = N.random.uniform(0,2*N.pi,n) * 180/N.pi

    x,y,z = lonlat2xyz(phi,theta)


    x,y,z = rotate_xyz(x,y,z,[(0,-90+lat),(lon,0)])

    phi,theta,rr = xyz2lonlat(x,y,z)

    phi = phi%360

    return phi,theta,area

def cap_area(radius):
    """ compute area of spherical cap"""
    r = radius*N.pi/180
    area = 2*N.pi*(1-N.cos(r))
    return area


def test_rotate_lonlat():
    lon = N.array([0,0,0,0])
    lat = N.array([0,30,45,90,])

    
    a,b = rotate_lonlat(lon,lat,angles=[(10,0)])
    assert(N.allclose(b,lat))
    assert(N.allclose(a,10))

    a,b = rotate_lonlat(lon,lat,angles=[(0,-10)])
    assert(N.allclose(b,lat-10))
    assert(N.allclose(a,0))

    lon = N.array([10,10,10,10])
    lat = N.array([0,30,45,90,])


    a,b = rotate_lonlat(lon,lat,angles=[(-10,-10),(10,0)])
    assert(N.allclose(b,lat-10))
    assert(N.allclose(a,lon))


if __name__=="__main__":
    print distance((0,0),(0,45))
    print distance((0,0),(10,0))
