mask files

field_W?.mangle      -- geometry of CFHTLS fields
mask_W?.c.reg.mangle -- holes due to stars and CCD gaps
