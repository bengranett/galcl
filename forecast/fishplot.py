import pylab
import numpy as N
from matplotlib.patches import Ellipse
from matplotlib.ticker import MaxNLocator,MultipleLocator,AutoMinorLocator

import utils

def fishplot(C, center=None, labels=None, ascale=1.52, color='hotpink',facecolor='None',dashes=(None,None),lw=1,alpha=1.,labeloffset=0,label=True, plot1d=True):
    """ """
    det = N.linalg.det(C)
    print "> matrix determinant",det


    ax = pylab.gca()
    n = len(C)

    if center is None:
        center = N.zeros(n)

    if labels is None:
        labels = range(n)

    sig = N.sqrt(N.diagonal(C))
    print "> sigmas:",sig
    limits = []
    for i,s in enumerate(sig):
        limits.append((center[i]-3*s, center[i]+3*s))

    patches = []
    for i in range(n):
        for j in range(n):
            if i>j: continue

            ax = pylab.subplot(n,n,j*n+i+1)
            
            if j==n-1: pylab.xlabel(labels[i])
            if i==0: pylab.ylabel(labels[j])

            if j < n-1:
                ax.set_xticklabels([])
            if i > 0:
                ax.set_yticklabels([])



            if i==j:
                if not plot1d: 
                    line, = pylab.plot([100,101],[100,101],c=color,lw=lw,dashes=dashes)    

                    continue
                sig = N.sqrt(C[i,i])
                x = N.linspace(limits[i][0],limits[i][1],100)
                y = N.exp(-(x-center[i])**2/2./sig**2)
                y /= N.sum(y) *(x[1]-x[0])
                line, = pylab.plot(x,y,c=color,lw=lw,dashes=dashes)    
                pylab.xlim(*limits[i])
                pylab.ylim(0,y.max()*2)
                ax.set_yticklabels([])

                if label: pylab.text(.05,0.8-labeloffset,"$\sigma = %3.4f$"%sig, transform=ax.transAxes)

                continue

            v00 = C[i,i]
            v11 = C[j,j]
            v01 = C[i,j]

            s = (v00+v11)/2.
            d = N.sqrt((v00-v11)**2/4. + v01**2)

            a = ascale*N.sqrt(s + d)
            b = ascale*N.sqrt(N.abs(s - d))

            

            th = 0.5*N.arctan2(2*v01,(v00-v11)) * 180/N.pi
            if dashes[0] is None:
                linestyle=None
            else:
                linestyle = (0,dashes)
            e = Ellipse(xy=(center[i],center[j]), width=2*a, height=2*b, angle=th, 
                facecolor=facecolor, lw=lw,linestyle=linestyle,edgecolor=color,alpha=alpha)

            ax.add_artist(e)

            pylab.xlim(*limits[i])
            pylab.ylim(*limits[j])


    for i in range(n):
        for j in range(n):
            if i>j: continue

            ax = pylab.subplot(n,n,j*n+i+1)

            ax.xaxis.set_major_locator(MaxNLocator(3))
            ax.xaxis.set_minor_locator(AutoMinorLocator(5))
            ax.yaxis.set_major_locator(MaxNLocator(3))
            ax.yaxis.set_minor_locator(AutoMinorLocator(5))

    pylab.subplots_adjust(hspace=0,wspace=0)
    return line




def sample_cov(C,center,m=10000):
    """ """
    sig = N.sqrt(N.diagonal(C))
    limits = []
    for i,s in enumerate(sig):
        limits.append((center[i]-2*s, center[i]+2*s))



    data = N.random.multivariate_normal(center, C, m)
    n = len(C)
    
    for i in range(n):
        for j in range(n):
            if i>j: continue

            ax = pylab.subplot(n,n,j*n+i+1)
            binx = N.linspace(limits[i][0],limits[i][1],20)
            biny = N.linspace(limits[j][0],limits[j][1],20)

            if i!=j:
                h,ey,ex = N.histogram2d(data[:,j],data[:,i],bins=(biny,binx))
                ll = utils.lowerwater(h,[0.68,0.95])
                pylab.contour(h, extent=(ex[0],ex[-1],ey[0],ey[-1]),levels=ll)
                pylab.plot(data[:,i],data[:,j],",")



if __name__=="__main__":
    fig = pylab.figure()

    C = N.diag([1.0,1.0,1.0])


    center=(0,0,0)

    fishplot(C,center)
    sample_cov(C,center)
    pylab.show()

