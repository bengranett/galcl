import numpy as N
import scipy
import scipy.interpolate

def KLDistance(p, q):
    """Kullback-Leibler divergence of Q from P.
    P is the reference or true distribution and Q is an estimate. """

    if not N.all(q>=0):
        return -1

    assert(len(p)==len(q))
    assert(N.all(q>=0))
    assert(N.all(p>=0))

    np = N.max(p)
    nq = N.max(q)

    ii = p>0
    t = q[ii]
    t[t<=0]=1e-10*nq

    if not N.all(t>=0):
        return -1


    assert(N.all(t>0))
    assert(N.all(p[ii]>0))

    np = N.sum(p[ii])
    nq = N.sum(t)

    d = N.sum(p[ii]*1./np*N.log(p[ii]*1./np)) - N.sum(p[ii]*1./np*N.log(t*1./nq))

    return d


def lowerwater(z, plevels,alpha=0.99):
    """ """
    if N.max(z)==0:
        return [0]*len(plevels)
    tot = N.sum(z)
    levels = []
    for l in plevels:
        h = z.max()
        t = tot*l
        ll = 0
        if z.sum()<t:
            levels.append(0)
        while z[z>h].sum()<t:
            h*=alpha
            ll+=1
            if ll>1e6:
                print "uhoh!",ll,h,t
                break

        levels.append(h)

    return levels


def conf_interval(z,pdf,pvalues=[0.95]):
    """ """
    zz = N.linspace(z.min(),z.max(),len(z)*10)
    interp = scipy.interpolate.interp1d(z,pdf)
    pdfi = interp(zz)

    ll = lowerwater(pdfi, pvalues)
    out = []
    for l in ll:
        ii = pdfi>l
        s = zz[ii]
        if len(s)==0: return [-1]*len(pvalues)
        a = s.min()
        b = s.max()
        out.append(b-a)
    return out


def moments(x,pdf,axis=-1):
    """Compute mean and variance of a distribution
    
    Parameters
    ----------
    x   : array
    pdf : array
    axis : int, optional
    """
    norm = N.sum(pdf)
    if norm <= 0:
        return 0., 0.

    norm = 1./norm

    mean = norm*N.sum(x*pdf, axis=axis)

    var = N.zeros(len(mean))
    for i in range(len(var)):
        d = x-mean[i]
        var[i] = norm*N.sum(d**2*pdf[i])

    return mean, var




class Base:
    params = {'verbose': True}
    def _message(self, m):
        """ """
        if self.params['verbose']:
            print "> %s: %s"%(self.__class__.__name__, m)
