import numpy as N
import pylab

import fish
import fishplot

if __name__=="__main__":
    """ """
    pylab.figure(figsize=(3,3))

    bias = 1.4

    styles = [(None,None),(4,1)]
    counter = 0

    for bias in [1.5]:

        ls = styles[counter]
        counter += 1

        F = fish.Fish(growthf=0.8,zmean=1.0, zsig=0.15,
                      pk_kmin=0.05, pk_kmax=0.21, pk_deltak=.04,
                      ngal_spec=10000, 
                      ngal_phot=1e5,
                      bias=bias)
        
        F_pk = F.gofish()
        F_mo = F.gofish(multipoles=(0,))
        F_qu = F.gofish(multipoles=(2,))
        F_cl = F.Clfish()
        
        F_cl = N.diag([F_cl, 0.01, 0.01])

        F_joint = F_pk + F_cl

        F_mo[0,0] += 0.01



        C_pk = N.linalg.inv(F_pk)
        C_joint = N.linalg.inv(F_joint)
        C_cl = N.linalg.inv(F_cl)
        C_mo = N.linalg.inv(F_mo)
        C_qu = N.linalg.inv(F_qu)

        corrcoeff = C_pk[0,1]/(C_pk[0,0]*C_pk[1,1])**.5
        print "corr coeff",corrcoeff


        fid = [F.fishparams['bias'],F.fishparams['growthf'],F.fishparams['sigmav']]

        patch0=fishplot.fishplot(C_joint,center=fid,labels=("$b$","$f$","$\sigma_v$"),facecolor='None',lw=1,color='firebrick',label=True)
        patch1=fishplot.fishplot(C_mo,center=fid,labels=("$b$","$f$","$\sigma_v$"),facecolor='None',lw=1,color='orange',label=False, plot1d=False)
        patch2=fishplot.fishplot(C_qu,center=fid,labels=("$b$","$f$","$\sigma_v$"),facecolor='None',lw=1,color='orange',dashes=(4,1),label=False, plot1d=False)
        patch3=fishplot.fishplot(C_cl,center=fid,labels=("$b$","$f$","$\sigma_v$"),facecolor='None',lw=1,color='hotpink',label=False, plot1d=False)
        patch4=fishplot.fishplot(C_pk,center=fid,labels=("$b$","$f$","$\sigma_v$"),facecolor='None',dashes = ls, lw=1,color='dodgerblue',label=True,labeloffset=0.15)

    pylab.figlegend([patch0,patch1,patch2,patch3,patch4],["P_0+P_2+Cl","P_0","P_2","Cl","P_0+P_2"],"upper right",frameon=False,fontsize=6)

    pylab.subplots_adjust(left=0.2,bottom=0.2)

    pylab.savefig("plot.pdf")
