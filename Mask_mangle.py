
import utils
from Mask import Mask
import mangle

class MangleMask(Mask):
    """ Routines to work with a mangle mask """

    def add_mask_file(self, mask_path, holes=False):
        """ """          
        if self.verbose: print "> loading mask path:",mask_path

        m = mangle.Mangle(mask_path)

        if holes:
            self.hole_masks.append(m)
        else:
            self.field_masks.append(m)

    def check_inside(self, ra, dec):
        """ Check if points are contained in the mask and outside the holes. """
        field_sel = True
        for m in self.field_masks:
            ii = m.contains(ra,dec)
            field_sel = N.logical_and(field_sel, ii)

        hole_sel = False
        for m in self.hole_masks:
            ii = m.contains(ra,dec)
            hole_sel = N.logical_or(hole_sel, ii)

        r = N.logical_and(field_sel, N.logical_not(hole_sel))

        return r
