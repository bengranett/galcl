import sys
import os.path

import pylab
import numpy as N
import pickle
import time

# cosmopy
import theory
import theory.pt as Pt
import theory.frw as Frw
import theory.param as Param
import theory.utils as Utils
import theory.proj as Proj
import theory.bessint as Bessint

import params

cambpath = params.camb_path
"""Do all sorts of calculations needed for projected power spectra."""


def loaddNdz(fname="dndz2salq_spline.txt"):
    """ """
    print "loading dNdz from", fname
    zz= []
    dndz = []
    for line in file(fname):
        z,d = [float(v) for v in line.split()]
        zz.append(z)
        dndz.append(d)
    dndz = N.array(dndz)
    return N.array(zz),dndz

def modeldNdz(beta = 1.723, lam=1.684, z0 = 0.084):
    """From Afshordi et al"""
    zz = N.arange(0.001,2.,0.001)
    #dndz = beta*(zz/z0)**(beta*lam - 1) * N.exp(-(zz/z0)**beta)
    dndz = beta*(zz/z0)**(lam) * N.exp(-(zz/z0)**beta)
    norm = Utils.trapz(dndz,zz)

    return zz,dndz/norm

def modelafshordi(z0, beta, lam):
    """From Afshordi et al"""
    zz = N.arange(0.001,1.,0.001)
    dndz = (zz/z0)**(beta*lam - 1) * N.exp(-(zz/z0)**beta)
    norm = Utils.trapz(dndz,zz)
    return zz,dndz/norm



def modeldNdzBlake(mu = 0.474, sigma=.035):
    """ """
    zz = N.arange(0.005,1.,0.001)
    dndz = N.exp(-(zz-mu)**2/2./sigma**2)
    norm = Utils.trapz(dndz,zz)
    return zz,dndz/norm


def sigma8(sig8_0, z, PS):
    """scale sigma8 to redshift z, from 0"""
    return sig8_0*PS.d1(z)/PS.d1(0)


class ISWCalculator:
    """ """
    def __init__(self, dNdz, kpk=None, bias=1.0, cambz = 0.55, nMax = 100, zmax=20, sig8=0.8,
                 params=None, runcamb=True, verbose=False):
        """ """
        self.dNdz = dNdz
        self.nMax = nMax
        self.bias = bias

        if params == None:
            self.Params = Param.CambParams()
        else:
            self.Params = Param.CambParams(**params)

        self.Params['output_root'] = 'camb_%i'%os.getpid()
            
        self.verbose = verbose
        if self.verbose: print self.Params
            
        self.temp_cmb = self.Params['temp_cmb'] * 1000 # in mK

        self.D = Frw.Distance(self.Params,zmax=zmax)
        print "done"
        self.PS = Pt.PowerSpectrum(self.Params)

        self.G0 = self.PS.d1(cambz)

        if not kpk == None:
            k,pk = kpk
            self.pk = pk/self.D.h**3
            self.k = k*self.D.h
            self.lk = N.log(self.k)
            return


        if not runcamb:
            return

        do_nonlinear = self.Params['do_nonlinear']
        self.Params['transfer_kmax'] = 100
        self.Params['transfer_k_per_logint'] = 2
        self.Params['transfer_redshift'] = [0]
        self.Params['do_nonlinear'] = 0

        # run CAMB twice to get amplitude correct
        camb = Pt.Camb(cambPath=cambpath, 
                       iniName = 'cosmopy_camb_%i.ini'%os.getpid(),
                       **self.Params)
        t0 = time.time()
        camb.run()
        print "camb time:",time.time()-t0

        sig2 = Pt.sigma2fromPk(camb,8)[0]
        print "sig8",N.sqrt(sig2)

        amp = sig8**2/sig2
        print sig8
        print "amp factor",amp
        self.Params['scalar_amp'][0] *= amp
        self.Params['transfer_redshift'] = [cambz]
        self.Params['do_nonlinear'] = do_nonlinear

        camb = Pt.Camb(cambPath=cambpath,     
                       iniName = 'cosmopy_camb_%i.ini'%os.getpid(),
                       **self.Params)
        t0 = time.time()
        camb.run()
        print "camb time:",time.time()-t0

        camb.logk,camb.logpk = N.log(camb.k),N.log(camb.pk)
        import scipy.signal as SS
        camb.pkSplineCoeff = SS.cspline1d(camb.logpk)

        #camb.kextend(-10,60)

        self.pk = camb.pk/self.D.h**3
        self.k = camb.k*self.D.h
        self.lk = N.log(self.k)

    def CmbCl(self, lvec):
        """ """
        filename = "tmpCosmoPyCamb_scalCls.dat"
        els = []
        cl = []
        for line in file(filename):
            nums = line.split()
            els.append(int(nums[0]))
            cl.append(float(nums[1]))
        return N.array(cl)[lvec]

    def getPhi(self, growth=True, dNdz=None):
        """ """
        if dNdz==None:
            z,dNdz = self.dNdz
        else:
            z,dNdz = dNdz
        r = self.D.rtc(z)
        phi = dNdz/self.D.vc(z)
        norm = Utils.trapz(r**2*phi, r)
        print "normalization check", Utils.trapz(dNdz,z),norm
        phi /= norm
        # multiply phi with the growth factor...

        if growth:
            print "inside ISWCalc, bias=",self.bias
            G = self.PS.d1(z)/self.G0
            phi *= G * self.bias

        # pad with zeros so extrapolation goes to 0
        phi = N.concatenate([[0,0],phi,[0,0]])

        dz = z[1]-z[0]
        z = N.concatenate([[z[0]-2*dz,z[0]-dz], z, [z[-1]+dz,z[-1]+2*dz]])
        func = lambda x: Utils.interpolateLin(phi, self.D.rtc(z), x)

        return func

    def dGrowth_thesame(self):
        """ """
        zz = N.arange(0, 19.9, .001)
        G = self.PS.d1(zz)
        dGda = (G[1:]-G[:-1])/(1/(1+zz[1:])-1/(1+zz[:-1]))
        z = .5*(zz[1:]+zz[:-1])
        dgrowth=-self.D.H0*self.D.E(z)/Frw.const.c*(dGda/(1+z)-self.PS.d1(z))
        
        dfunc =  lambda x: Utils.interpolateLin(dgrowth, self.D.rc(z), x)
        return dfunc



    def dGrowth(self):
        """ """
        zz = N.arange(0.005, 19.9, .001)
        G = self.PS.d1(zz)/self.G0
        r = self.D.rtc(zz)

        #pylab.plot(r,G*(1+zz))
        #pylab.figure()
        
        g = G*(1+zz)
        
        dgdr = (g[:-1]-g[1:])/(r[:-1]-r[1:])
        dgdr = N.concatenate([[0,0],dgdr,[0,0]])

        zz = 0.5*(zz[1:]+zz[:-1])

        dz = zz[1]-zz[0]
        zz = N.concatenate([[zz[0]-2*dz, zz[1]-dz], zz, [zz[-1]+dz,zz[-1]+2*dz]])
        r = self.D.rtc(zz)

        #pylab.plot(zz, dgdr)
        #pylab.show()
        dfunc =  lambda x: Utils.interpolateLin(dgdr, r, x)
        return dfunc

    def GalaxyCl_bess(self, lvec, zdistort = True, beta=0.3):
        """ """
        phifunc = self.getPhi()
        
        cl = []
        W = []
        for l in lvec:
            print "l=",l
            B = Bessint.BesselIntegrals(l+.5, self.nMax)
            if l > 1:
                BP = Bessint.BesselIntegrals(l+2.5, self.nMax)
                BN = Bessint.BesselIntegrals(l-1.5, self.nMax)
            W = []
            W_z = []
            for k in self.k:
                f_gal = lambda x: x**2/k**3 * phifunc(x/k) * N.sqrt(N.pi/2./x)
                step = (self.D.rtc(0.6)-self.D.rtc(0.5))*k/5000.
                W_gal = B.besselInt(f_gal, self.nMax, step)


                if zdistort and l > 1:
                    # redshift distortion correction from Padmanabhan 2006
                    W_z = -(l+1)*(l+2)/(2.*l+1)/(2.*l+3)*BP.besselInt(f_gal, self.nMax, step)
                    W_z += -l*(l-1)/(2.*l-1)/(2.*l+1)*BN.besselInt(f_gal, self.nMax, step)
                    W_z += (2*l**2+2*l-1)/(2.*l+3)/(2.*l-1)*W_gal
                    
                    W_gal += beta*W_z
                W.append(W_gal**2)

            W = N.array(W)
            func = self.k**3*self.pk*W
            cl.append(2./N.pi * Utils.trapz(func,self.lk))
        return N.array(cl)

    def IswCl(self,lvec):
        """
        """
        phifunc = self.getPhi()
        dfunc = self.dGrowth()
        cl = []
        W = []

        norm = self.temp_cmb * 3*self.D.m0*self.D.cH**-2
        
        for l in lvec:
            print l
            B = Bessint.BesselIntegrals(l+.5, self.nMax)
            W = []
            wisw = []
            wgal = []
            for k in self.k:
                f_isw = lambda x: norm/k**3 * dfunc(x/k) * N.sqrt(N.pi/2./x)
                step = k /10.
                W_isw = B.besselInt(f_isw, self.nMax, step)
                
                f_gal = lambda x: x**2/k**3 * phifunc(x/k) * N.sqrt(N.pi/2./x)
                step = (self.D.rtc(0.6)-self.D.rtc(0.5))*k/5000.
                W_gal = B.besselInt(f_gal, self.nMax, step)

                W.append(W_isw * W_gal)
                wisw.append(W_isw)
                wgal.append(W_gal)

            W = N.array(W)
            func = self.k**3*self.pk*W
            cl.append(2./N.pi * Utils.trapz(func,self.lk))

        return N.array(cl)

    def IswTempCl(self,lvec):
        """
        """
        dfunc = self.dGrowth()
        cl = []
        W = []

        norm = self.temp_cmb * 3*self.D.m0*self.D.cH**-2
        
        for l in lvec:
            print l
            B = Bessint.BesselIntegrals(l+.5, self.nMax)
            W = []
            for k in self.k:
                f_isw = lambda x: norm/k**3 * dfunc(x/k) * N.sqrt(N.pi/2./x)
                step = k/10.
                W_isw = B.besselInt(f_isw, self.nMax, step)                
                W.append(W_isw**2)

            W = N.array(W)
            func = self.k**3*self.pk*W
            cl.append(2./N.pi * Utils.trapz(func,self.lk))

        return N.array(cl)


    def IswTempCl_flat(self,lvec, zrange=[.01,10], kfilter=None):
        """
        """
        zz = N.arange(zrange[0],zrange[1],.005)
        r = self.D.rc(zz)

        phifunc = self.getPhi()
        dfunc = self.dGrowth()
        cl = []
        W = []

        growth = self.PS.d1(zz)/self.G0
        #pylab.figure()
        #pylab.plot(zz,growth)
        #pylab.show()

        norm = self.temp_cmb * 3*self.D.m0*self.D.cH**-2

        
        if not kfilter == None:
            pk = self.pk * kfilter(self.k)
            pylab.figure()
            pylab.semilogx(self.k,  kfilter(self.k))
            pylab.show()
        else:
            pk = self.pk
        
        for l in lvec:
            f = norm *r**2*dfunc(r)/(l+.5)**2
            ff= f**2/r**2 * growth**2 * Utils.interpolateLin(pk,self.lk,N.log((l+0.5)/r))
            I = Utils.trapz(ff, r)

            cl.append(I)

        cl[0] = 0.
        return N.array(cl)

    

    def IswCl_flat(self, lvec):
        """ """
        zz = N.arange(0.1,1,.005)
        r = self.D.rc(zz)
        phi = self.getPhi()

        dgrowth = self.dGrowth()

        zz = N.arange(0.1,1,.005)
        r = self.D.rc(zz)

        cl = []
        for l in lvec:
            f = phi(r)*dgrowth(r)*r**2*Utils.interpolateLin(self.pk,self.lk,N.log((l+0.5)/r))
            I = Utils.trapz(f, r)
            cl.append(self.temp_cmb*3/self.D.cH**2*self.D.m0*I/(l+0.5)**2)

        return N.array(cl)


    def TempCl(self, lvec):
        """ """
        zz = N.arange(0.1,1,.005)
        r = self.D.rc(zz)
        
        phi = self.getPhi()
        
        dgrowth = self.dGrowth()

        zz = N.arange(0.1,1,.005)
        r = self.D.rc(zz)

        cl = []
        for l in lvec:
            f = phi(r)*dgrowth(r)*r**2*Utils.interpolateLin(self.pk,self.lk,N.log((l+0.5)/r))
            I = Utils.trapz(f, r)
            cl.append(self.temp_cmb*3/self.D.cH**2*self.D.m0*I/(l+0.5)**2)

        return N.array(cl)



    def GalaxyCl_flat(self, lvec, zmax=2, dndz2=None, growth=True):
        """ """
        zz = N.arange(0.0001,zmax,.0001)
        r = self.D.rc(zz)
        
        phi = self.getPhi(growth=growth)
        if not dndz2==None:
            phi2 = self.getPhi(dNdz=dndz2, growth=growth)
        else:
            phi2 = phi
        
        cl = []
        for l in lvec:
            f = phi(r)*phi2(r)*r**2*Utils.interpolateLin(self.pk,self.lk,N.log((l+0.5)/r))
            I = Utils.trapz(f, r)
            cl.append(I)

        return N.array(cl)

    def GalaxyCl_kint(self, lvec,zmax=2):
        """ Test to compute the galaxy Cl's with an integral over k. """
        phi = self.getPhi()
        rmax = self.D.rc(zmax)
        cl = []
        for l in lvec:
            r = l*1./self.k
            #f = phi(r)**2*r**4

            
            f = 1./l*phi(r)**2*r**4*self.k*self.pk
            I = Utils.trapz(f,self.lk)
            cl.append(I)
        return N.array(cl)

    def kernel(self, z):
        """ """
        r = self.D.rc(z)
        phi = self.getPhi()
        f = phi(r)**2*r**4
        return r,f
    

    def GalaxyCl(self, lvec, cuta=50, cutb=50, cachefile="cache/GalPowerSpectrum.pickle"):
        """ """
        if os.path.exists(cachefile):
            try:
                rlvec,rcl, rbias = pickle.load(file(cachefile,'r'))
                if N.all(rlvec == lvec) and self.bias == rbias:
                    print "loaded cached galaxy Cls"
                    return rcl
            except:
                pass
        
        a = lvec[N.where(lvec < cuta)]
        b = lvec[N.where(lvec >= cuta)]
        c = b[N.where(b < cutb)]
        d = b[N.where(b >= cutb)]

        cla = self.GalaxyCl_bess(a, zdistort=True)
        clb = self.GalaxyCl_bess(c, zdistort=False)
        clc = self.GalaxyCl_flat(d)

        r = N.concatenate([cla,clb,clc])
        pickle.dump((lvec,r,self.bias), file(cachefile,'w'))

        return r


    def SZCl_flat(self, lvec):
        """SZ cross-cor with galaxies."""

        zz = N.arange(0.1,1,.005)
        r = self.D.rc(zz)
        
        phi = self.getPhi()
        
        cl = []
        nu = 40 # frequency in GHz
        x = 0.04799237 * nu / 2.73
        f = 1.16e-4*self.Params['omega_baryon']*self.D.h*(4-x/N.tanh(x/2))/self.D.cH

        D = self.PS.d1(zz)/self.G0
        
        for l in lvec:
            integ = -f*(1+zz)**2*phi(r)*Utils.interpolateLin(self.pk,self.lk,N.log((l+0.5)/r))            
            I = Utils.trapz(integ, r)
            cl.append(self.temp_cmb*I)

        return N.array(cl)

    def darkflow(self, lvec):
        """SZ cross-cor with galaxies."""

        zz = N.arange(0.001,1.,.001)
        r = self.D.rc(zz)
        
        phi = self.getPhi()
        
        cl = []
        ftg = 1./3
        
        ne = 9.83*self.Params.ombh2  # m^-3
        sigT = 6.652e-29      # m^2
        v0 = 1.e6  # m/s
        H = 9.7776e9/self.D.h*3.155815e7  #1/H0 in seconds
        f = ne*sigT*v0*H
        print "6.1e-6=",f
        f *= 1./self.D.cH

        cl = N.zeros(len(lvec),dtype='d')
        for i,l in enumerate(lvec):
            integ = (1+zz)**2*phi(r)*Utils.interpolateLin(self.pk,self.lk,N.log((l+0.5)/r))
            I = f*ftg*Utils.trapz(integ, r)
            cl[i] = I

        print "Tcmb=",self.temp_cmb
        cl *= self.temp_cmb 
        return cl



def testISW():
    #import lrgtheory
    ls = N.arange(200)

    dNdz = loaddNdz()

    isw = ISWCalculator(dNdz, bias=1.8,cambz=0.5,nMax=1000)

            
    cl_flat = isw.IswCl_flat(ls)
    pylab.semilogx(ls,ls*(ls+1)*cl_flat/2./N.pi, label="flat sky")


    ls = N.arange(20)
    cl = isw.IswCl(ls)
    pylab.semilogx(ls,ls*(ls+1)*cl/2./N.pi,".-", label="bessel integral")


    pylab.legend()
    #pylab.figure()

    
    #pylab.semilogx(ls,cl_flat)

    #pylab.plot(ls,cl)
    pylab.show()


def testGal():
    """ """    
    #import lrgtheory
    ls = N.arange(2,20)

    dNdz = loaddNdz()

    isw = ISWCalculator(dNdz, bias=3,cambz=0,nMax=100)


    cl = isw.GalaxyCl(ls)
    pylab.loglog(ls,ls*(ls+1)*cl/2./N.pi, label="all")

    
    cl_flat = isw.GalaxyCl_flat(ls)
    pylab.loglog(ls,ls*(ls+1)*cl_flat/2./N.pi, label="flat sky")


    ls = N.arange(2,20)
    #cl = isw.GalaxyCl_bess(ls)

    #cl_noz = isw.GalaxyCl_bess(ls, zdistort=False)

    #pylab.loglog(ls,ls*(ls+1)*cl/2./N.pi,".-", label="redshift dist")
    #pylab.loglog(ls,ls*(ls+1)*cl_noz/2./N.pi,".", label="bessel integral")


    pylab.legend()
    #pylab.figure()

    
    #pylab.semilogx(ls,cl_flat)

    #pylab.plot(ls,cl)
    pylab.show()


def testSZ():

    ls = N.arange(10000)

    #dNdz = loaddNdz()
    #dNdz = modeldNdz()
    dNdz = modeldNdz(beta = 1.68045, lam=1.85379, z0 = 0.065)

    isw = ISWCalculator(dNdz)


    #cl = isw.SzCl_flat(ls)*1000
    cl = isw.darkflow(ls)

    out = file('darkflowfilter.dat','w')
    for l in range(len(cl)):
        print >>out, l, cl[l]
    out.close()

    #pylab.semilogx(ls,ls*(ls+1)*cl/(2.*N.pi), label="sz")
    pylab.semilogx(ls,cl, label="sz")

    pylab.legend()

    pylab.show()


def temperatureISW():

    omega_c = 0.233
    omega_b = 0.0462
    h = 0.701
    doubleu=-1



    cosmology = Param.CosmoParams(hubble=h*100., omega_cdm=omega_c,
                                  omega_baryon = omega_b, w = doubleu,
                                  omega_lambda = 1.-omega_c-omega_b,
                                  omch2=omega_c*h**2,
                                  ombh2=omega_b*h**2,
                                  omk=0,
                                  )

    
    ls = N.arange(1000)
    la = N.arange(50)

    dNdz = modeldNdz()

    isw = ISWCalculator(dNdz, bias=1.,cambz=0.,nMax=1000,params=cosmology)

            
    #cl = isw.IswTempCl(la) * 1e6
    #pylab.loglog(la,la*(la+1)*cl/2./N.pi, label="ISW")

    #cla = cl

    cl = isw.IswTempCl_flat(ls)*1e6
    pylab.loglog(ls,ls*(ls+1)*cl/2./N.pi, label="ISW")

    cl = isw.IswTempCl_flat(ls,zrange=[0.4,.6])*1e6
    pylab.loglog(ls,ls*(ls+1)*cl/2./N.pi, label="ISW 0.4-0.6")

    
    pylab.show()
    sys.exit()
    import wmap
    W = wmap.WMAP()
    cl = W.readBestFitCls()
    cl = cl[:1000]

    el = N.arange(2,len(cl)+2)

    print el.shape
    print cl.shape
    
    pylab.loglog(el, cl, label="CMB")
    
    pylab.legend(loc='lower left')

    pylab.xlabel("$l$")
    pylab.ylabel("$l(l+1) C_l / 2\pi\  \mu K^2$")

    pylab.xlim(0,1e3)

    pylab.savefig("iswcl.png")
    
    pylab.show()


if __name__=="__main__":
    #temperatureISW()
    #testISW()
    testSZ()

