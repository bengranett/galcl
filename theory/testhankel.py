import numpy as N
import pylab as M
import pt
import hankel
import utils

def testhankel():
    # millennium simulation params
    c = pt.Camb(cambPath="/home/granett/cosmopy/CAMB/",hubble=73., ombh2 = (0.25-0.045)*0.7**2, omch2 = 0.25*0.7**2)
    c.run()
    sig82 = pt.normalizePk(c,0.9)

    hank = hankel.Hankel3D(10000)

    r = 2**N.arange(0.01,1,0.01)
    xi256 = hank.transform(c.pkInterp,r, n=10000,h=1./256.)
    xi1000 = hank.transform(c.pkInterp,r, n=1000,h=1./512.)
    xi = hank.transform(c.pkInterp,r, n=10000,h=1./512.)
    
    M.plot(r,xi256)
    M.plot(r,xi1000)
    M.plot(r,xi)
    M.show()

if __name__=="__main__":
    testhankel()
