import pylab as M
import numpy as N
import scipy.interpolate as SI

class LogInterp:
    """
    
    """

    def __init__(self,x,f):
        """

        """
        self.xa = x
        self.fa = f
        n = x.shape[0]
        self.n = n
        self.xMin = x[0]
        self.xMax = x[n-1]
        lnx = M.log(x)
        lnf = M.log(f)
        self.fSpl = SI.splrep(lnx,lnf,s=0)
        self.nAtxMin = (lnf[1]-lnf[0])/(lnx[1]-lnx[0])
        self.fAtxMin = f[0]
        self.nAtxMax = (lnf[n-1]-lnf[n-2])/(lnx[n-1]-lnx[n-2])
        self.fAtxMax = f[n-1]

    def f1(self,x):
        return self.fAtxMin*(x/self.xMin)**self.nAtxMin        

    def f2(self,x):
        if len(x)<1:
            return
        lnx = M.log(x)
        lnf = SI.splev(lnx,self.fSpl,der=0)
        return M.exp(lnf)
    
    def f3(self,x):
        return self.fAtxMax*(x/self.xMax)**self.nAtxMax

    def f(self,x):
        if N.isscalar(x):
            x = M.array([x])
        return N.piecewise(x,[x<=self.xMin,x>=self.xMax],[self.f1,self.f3,self.f2])



class LinInterp:
    def __init__(self,x,f):
        """
        
        """
        self.xa = x
        self.fa = f
        n = x.shape[0]
        self.n = n
        self.xMin = x[0]
        self.xMax = x[n-1]
        self.fSpl = SI.splrep(x,f,s=0)
        self.nAtxMin = (M.log(f[1])-M.log(f[0]))/(M.log(x[1])-M.log(x[0]))
        self.fAtxMin = f[0]
        self.nAtxMax = (M.log(abs(f[n-1]))-M.log(abs(f[n-2])))/(M.log(x[n-1])-M.log(x[n-2]))
        self.fAtxMax = f[n-1]

    def f1(self,x):
        return self.fAtxMin*(x/self.xMin)**self.nAtxMin        

    def f2(self,x):
        if len(x)<1:
            return
        return SI.splev(x,self.fSpl,der=0)
    
    def f3(self,x):
        return self.fAtxMax*(x/self.xMax)**self.nAtxMax

    def f(self,x):
        if N.isscalar(x):
            x = M.array([x])
        return N.piecewise(x,[x<=self.xMin,x>=self.xMax],[self.f1,self.f3,self.f2])
