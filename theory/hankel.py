import pylab as M
import numpy as N
import bessint as BI

class Hankel3D:
    def __init__(self,nMax=10000):
        self.bessint = BI.BesselIntegrals(0.5,nMax)

    def transform1(self,f,x,n,h,pk2xi=True):
        """
        Does the transform on a single x
        """
        bi = 1.0/x**3*self.bessint.besselInt(lambda z:z**1.5*f(z/x),n,h)
        pf = 1.0/(2.0*M.pi)**1.5
        if not(pk2xi):
            pf =1.0/pf
        return pf*bi

    def transform(self,f,x,n=1000,h=1./512.,pk2xi=True):
        """
        f is a function giving p(k), if transforming to xi(r), or
        xi(r), if transforming to p(k).  If you're transforming data,
        that means you should call a function that interpolates that
        data, e.g. utils.splineIntLinExt_LogLog().  The function will
        generally be evaluated well outside the range where it's
        interpolating, so be sure that it extrapolates sensibly,
        e.g. lim x->inf f(x) = 0.

        x is the array of abscissas of the result, e.g. if
        p(k)->xi(r), x will be the array of points where xi is to be
        evaluated.

        n is the number of Bessel-function zeroes to include.  As suggested,
        1000 probably will work, but test this.

        h is the stepsize of the integration. 1/512 seems to work.
        """
        if N.isscalar(x):
            return self.transform1(f,x,n,h,pk2xi)
        else:
            return M.array(map(lambda z:self.transform1(f,z,n,h,pk2xi),x))
