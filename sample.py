import sys,os
import numpy as N
from Mask import Mask
import maps
import sphere
import pylab
import cPickle as pickle

sample_count = 1
class Sample:
    """ """
    params = {'map_nside': 512,
              'map_order': 'nest',
              'map_completeness_thresh': 0.5,
              'randoms_ntimes': 10,
              'randoms_density': None
}
            

    def __init__(self, name=None, catalog=None, weights=None, zdist=None, 
                 bias=1.0,starfrac=0.,
                 center=None, radius=None, cachedir='cache/sample_%s', **params):
        """ 
        Define a galaxy sample

        Inputs
        ------

        Outputs
        -------

        """
        for k in params.keys():
            if self.params.has_key(k):
                self.params[k] = params[k]

        self.cachedir = None
        if cachedir is not None:
            self.cachedir = cachedir%name
            if not os.path.exists(self.cachedir):
                os.mkdir(self.cachedir)

        if name is None:
            global sample_count
            name = "sample%i"%sample_count
            sample_count += 1
        self.name = name
        self.bias = bias
        self.starfrac = starfrac

        self.catalog = catalog
        self.weights = weights
        self.zdist = zdist
        self.randoms = None

        self.delta_map = None
        self.mask_map = None
        self.nbar = None

        self.mask = None
        if self.catalog is not None:
            self.mask = Mask(center=center, radius=radius)

        print "> input params"
        print "> bias",self.bias
        print "> starfrac",self.starfrac

    def add_angular_field_mask_file(self, path, format="mangle"):
        """ 
        Inputs
        ------

        Outputs
        -------
        """
        if self.mask is None: 
            self.mask = Mask()
        self.mask.add_mask_file(path, holes=False, format=format)

    def add_angular_hole_mask_file(self, path, format="mangle"):
        """ 
        Inputs
        ------

        Outputs
        -------
        """
        if self.mask is None: 
            self.mask = Mask()
        self.mask.add_mask_file(path, holes=True, format=format)

    def plot_zdist(**params):
        """ """
        import pylab
        pylab.plot(self.zdist[0],self.zdist[1],**params)

    def get_nbar(self):
        """ """
        if self.nbar is not None:
            return self.nbar

        self.make_healpix_map()
        return self.nbar


    def make_healpix_map(self, fix_nbar=None):
        """ 
        Inputs
        ------

        Outputs
        -------

        """
        if self.randoms is None:
            self.random_sample()
            
        nbar_tag = ""
        if fix_nbar is not None:
            nbar_tag = "_n%i"%(fix_nbar*100)

        map_order = self.params['map_order']
        nside = self.params['map_nside']
        completeness_thresh = self.params['map_completeness_thresh']

        savefile1 = "%s/delta_%s%s.pickle"%(self.cachedir,self.name,nbar_tag)
        savefile2 = "%s/mask_%s%s.pickle"%(self.cachedir,self.name,nbar_tag)
        savefile3 = "%s/var_%s%s.pickle"%(self.cachedir,self.name,nbar_tag)
        savefile4 = "%s/stats_%s%s.pickle"%(self.cachedir,self.name,nbar_tag)

        if os.path.exists(savefile1):
            self.delta_map = maps.read_map(savefile1)
            self.mask_map = maps.read_map(savefile2)
            self.var_map = maps.read_map(savefile3)
            self.nbar, = pickle.load(file(savefile4))
            return self.delta_map, self.var_map

        # get requested ordering nest or ring
        nest = False
        if map_order.lower()[0] == 'n':
            nest = True

        # grid randoms
        rand = maps.healpixit(*self.randoms.T, nside=nside, nest=nest)

        # normalize random map by mean density to give completess map
        pixel_area = sphere.fullsky/(12*nside**2)
        dens = self.random_density*pixel_area
        compl = rand/dens

        compl[compl<completeness_thresh] = 0.

        # define weight=1/completeness
        ii = compl>0

        self.mask_map = N.zeros(compl.shape)
        self.mask_map[ii] = 1


        # grid galaxies
        gal = maps.healpixit(*self.catalog.T, nside=nside, nest=nest)

        # compute stats
        if fix_nbar is None:
            nbar = N.sum(gal[ii])/N.sum(compl[ii])
        else:
            nbar = fix_nbar

        # delta map
        delta_map = gal*0.0
        delta_map[ii] = gal[ii]/(compl[ii]*nbar) - 1
        delta_map *= 1./(1-self.starfrac)


        # variance map
        var_map = gal*0.0
        var_map[ii] = 1./(compl[ii]*nbar)

        var_map *= 1./(1-self.starfrac)**2

        self.delta_map = delta_map
        self.var_map = var_map
        self.nbar = nbar

        maps.dump_map(self.delta_map, self.mask_map, savefile1)
        maps.dump_map(self.mask_map, self.mask_map, savefile2)
        maps.dump_map(self.var_map, self.mask_map, savefile3)

        pickle.dump((nbar,), file(savefile4,"w"))

        return self.delta_map, self.var_map


    def random_sample(self):
        """ Generate random sample
        Inputs
        ------

        Outputs
        -------

        """
        ntimes = self.params['randoms_ntimes']
        density = self.params['randoms_density']

        savefile1 = "%s/rand_%s.npy"%(self.cachedir,self.name)
        savefile2 = "%s/area_%s.pickle"%(self.cachedir,self.name)

        if self.catalog is not None:
            n = ntimes*len(self.catalog)
        else:
            n = ntimes

        if os.path.exists(savefile1) and os.path.exists(savefile2):
            self.randoms = N.load(savefile1)
            self.area,self.random_density = pickle.load(file(savefile2))
            return self.randoms

        print "> random sample n=%i or dens=%s"%(n,str(density))

        ra,dec,area = self.mask.random_sample(n=n,density=density)
        self.randoms=N.transpose([ra,dec])

        print " mask area",area

        self.area = area
        self.random_density = len(ra)*1./area

        N.save(savefile1, self.randoms)
        pickle.dump((self.area,self.random_density), file(savefile2,"w"))

        return self.randoms
