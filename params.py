field_names = ['W1','W2','W3','W4']
# matched specz-photz catalog
catalog_spec_path = "data/cats/vipers_cat_photz.npy"
# raw photz catalog
catalog_path = "/scratch/ben/oobleckbackup/data/cfhtls/t07/photozCFHTLS-%s_270912.out"
# processed photz catalog through mask
catalog_path_cache = 'data/cats/t07cat.npy'
# randoms
randoms_path = 'data/randoms/cfhtls_randoms_%s.npy'

# zdist path
zdist_path = 'data/zdist/zdist.npy'

map_nside = 512
map_order = 'nested'
map_completenss_tresh = 0.5

photz_bins = [0.6,0.7,0.8,0.90,1.,2.]

starfrac = {'W1': 0.010,
            'W2': 0.036,
            'W3': 0.010,
            'W4': 0.034
        }

ntimes = 10

fields = {
        'W1':{'center': (34.5,-7.5),'radius':5.7, 
              'field_mask':('data/masks/field_W1.mangle','mangle'),
              'hole_mask':('data/merate_mask/photo_W1.reg','holey'),
              },
        'W2':{'center': (134.5,-3.3),'radius':3.5, 
              'field_mask':('data/masks/field_W2.mangle','mangle'),
              'hole_mask':('data/merate_mask/photo_W2.reg','holey'),
              },
        'W3':{'center': (214.5,54.5),'radius':7.0, 
              'field_mask':('data/masks/field_W3.mangle','mangle'),
              'hole_mask':('data/merate_mask/photo_W3.reg','holey')
              },
        'W4':{'center': (333.0,2.0),'radius':4.5, 
              'field_mask':('data/masks/field_W4.mangle','mangle'),
              'hole_mask':('data/merate_mask/photo_W4.reg','holey'),
              },
}

# cosmological parameters have CAMB compatible names
fiducial = {
        'ombh2': 0.02260,
        'omch2': 0.1123,
        'omnuh2': 0,
        'omk': 0,
        'hubble': 70.4,
        'w': -1,
        'cs2_lam': 1,
        'omega_baryon':    0.0456,
        'omega_cdm':       0.227,
        'omega_lambda':    0.7274,
        'omega_neutrino':  0,
        'temp_cmb': 2.726,
        'helium_fraction': 0.24,
        'massless_neutrinos': 3.,
        'massive_neutrinos': 0,
        're_redshift': 12,
        're_ionization_frac': 1,
        'scalar_amp': [2.3e-9],
        'scalar_spectral_index': [0.963],
        'scalar_nrun': [0],
        'tensor_spectral_index': [0],
        'initial_ratio': [1]
        }
camb_path = '/net/home/ben/camb'



cl_bands = range(0,1000,50)
cl_bands[0] = 2
