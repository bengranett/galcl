import pylab
import numpy as N

from sklearn.neighbors import KDTree


def main():
    zphot = N.load("data/cats/spec_zphot.npy")
    phot = N.load("data/cats/vipers_cat_photz.npy")
    mags = N.load("data/cats/phot_mags.npy")

    zspec = phot[:,3]

    print phot.shape
    print zspec.shape
    print mags.shape


    c1 = mags[:,0]-mags[:,1]
    c2 = mags[:,1]-mags[:,2]
    c3 = mags[:,2]-mags[:,3]
    c4 = mags[:,3]-mags[:,4]

    data_all = N.transpose([c1,c2,c3,c4,mags[:,3]])


    low,high = 0.9,2.0
    sel = (zphot>low)&(zphot<high)

    data = data_all[sel]
    zphot = zphot[sel]
    zspec = zspec[sel]

    tree = KDTree(data)

    width = N.zeros(len(zphot))
    mu = N.zeros(len(zphot))

    for i in range(len(zphot)):
        dist,matches = tree.query([data[i]], k=100)
        m = matches[0]
        z = zspec[m]
        width[i] = N.std(z)*1./(1+N.mean(z))
        mu[i] = N.mean(z)

    #pylab.hist(mu, bins=50)
    #pylab.figure()
    pylab.plot(mu,width,".")
    pylab.xlabel("redshift")
    pylab.ylabel("spread")

    m = N.percentile(mu,50)
    m1= N.percentile(width,80)
    pylab.axvline(m)
    pylab.axhline(m1)
    print "cut",m,m1
    ii = (mu > m) & (width < m1)

    zspec_hq = zspec[ii]



    bins = N.arange(0,2,.05)
    x = (bins[1:]+bins[:-1])/2.

    h,e = N.histogram(zspec,bins = bins,density=True)
    hq,e = N.histogram(zspec_hq,bins = bins,density=True)

    pylab.figure()
    pylab.plot(x,h)
    pylab.plot(x,hq)

    m = N.sum(x*h)/N.sum(h)
    mq = N.sum(x*hq)/N.sum(hq)
    print "means",m,mq
    print "stdd",N.sqrt(N.sum((x-m)**2*h)/N.sum(h))
    print "stdd",N.sqrt(N.sum((x-mq)**2*hq)/N.sum(hq))

    print "nspec",len(zspec)
    print "nspec hq",len(zspec_hq)

    pylab.figure()

    datahq = data[ii]
    c1,c2,c3,c4,mi = data.T
    c1h,c2h,c3h,c4h,mih = datahq.T

    labels = ['u-g','g-r','r-i','i-z','mag']
    for i in [0,1,2,3,4]:
        for j in [0,1,2,3,4]:
            if i>=j: continue
            pylab.subplot(5,5,5*(j)+i+1)
            pylab.plot(data_all.T[i],data_all.T[j],",",c='k')
            pylab.plot(data.T[i],data.T[j],".",c='grey')
            pylab.plot(datahq.T[i],datahq.T[j],".",c='r')

            pylab.xlabel(labels[i])
            pylab.ylabel(labels[j])
            if i<4: pylab.xlim(-.5,2.5)
            if j<4: pylab.ylim(-2,2)
            if j==4: pylab.ylim(19,22.9)

#    pylab.plot(data_all.T[2],data_all.T[3],",")
#    pylab.plot(c3,c4,".",c='grey')
#    pylab.plot(c3h,c4h,".",c='r')


    #pylab.plot(zphot,zspec,",",c='grey')
    #pylab.plot(zphot[ii],zspec[ii],"r,")



    pylab.show()






main()
