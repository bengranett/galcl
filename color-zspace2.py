import sys,os
import pylab
import numpy as N

import pickle

from sklearn.neighbors import KDTree
from sklearn import svm

class classifier:
    def __init__(self, traindata, trainflag, savefile="data/classifier.pickle"):
        """ """
        self.ready = False
        self.savefile = savefile

        if os.path.exists(savefile):
            print "> loading classifier from file",savefile
            self.machines = pickle.load(file(savefile))
            self.ready = True

        self.data = traindata
        self.flag = trainflag

        self.clf = svm.SVC(gamma=1)#class_weight='balanced')

        

    def run(self, frac=0.30):
        """ """    
        nsel = int(len(self.data)*frac)
        r = N.random.uniform(0,1,len(self.data)).argsort()

        set1 = r[:nsel]
        set2 = r[nsel:]

        train_data = self.data[set1]
        valid_data = self.data[set2]
        train_c = self.flag[set1]
        valid_c = self.flag[set2]

        print "  > training set",train_c.shape

        self.clf.fit(train_data,train_c)


        print "  > validation set", valid_c.shape

        r = self.clf.predict(valid_data)
        err = valid_c-r

        ii = valid_c==1

        print "  error",N.sum(ii),N.mean(err[ii])
        return r

    def train(self, ntimes=10):
        """ """
	if self.ready:
            print "> ready to go!"
            return
        pout = []
        machines = []
        for loop in range(ntimes):
            print "> training loop",loop
            self.run()
            machines.append(pickle.dumps(self.clf))
        self.machines = machines

        pickle.dump(machines, file(self.savefile,"w"))
        print "> classifier saved",self.savefile
        self.ready = True


    def predict(self, data):
        """ """
        if not self.ready:
            print "> classifier is not trained!"
            exit(-1)
        p = N.zeros(len(data),dtype='d')
        count = 1
        print "> classifying: ",
        for m in self.machines:
            sys.stdout.flush()
            clf = pickle.loads(m)
            a = clf.predict(data)
            print count,N.sum(a)
            p += a
            count += 1
        print ""

        return p*1./count

        


def main(zmin=1.0,zmax=2.0):
    """ """

    tag = "%i-%i"%(zmin*100,zmax*100)
    savefile = "data/classifier/classif_%s.pickle"%tag

    zphot = N.load("data/cats/spec_zphot.npy")
    vipers = N.load("data/cats/vipers_cat_photz.npy")
    mags = N.load("data/cats/phot_mags.npy")

    zspec = vipers[:,3]



    c1 = mags[:,0]-mags[:,1] # u-g
    c2 = mags[:,1]-mags[:,2] # g-r
    c3 = mags[:,2]-mags[:,3] # r-i
    c4 = mags[:,3]-mags[:,4] # i-z
    magi = mags[:,3]

    data = N.transpose([c1,c2,c3,c4,magi])

    c = N.zeros(zspec.shape)
    sel = (zspec >= zmin) & (zspec < zmax)
    c[sel] = 1

    print "> training set:",N.sum(c)


    clf = classifier(data,c,savefile=savefile)
    clf.train(ntimes=10)

    ii = clf.predict(data)>0.5


    vipers_sel = vipers[ii]

    N.save("data/vipers_sel_%s.npy"%tag, vipers_sel)
    

    zspec_sel = zspec[ii]



    bins = N.arange(0,2,0.01)
    x = (bins[1:]+bins[:-1])/2.

    h,e = N.histogram(zspec,bins = bins,density=True)
    hq,e = N.histogram(zspec_sel,bins = bins,density=True)

    pylab.plot(x,h)
    pylab.plot(x,hq)

    m = N.sum(x*h)/N.sum(h)
    mq = N.sum(x*hq)/N.sum(hq)
    print "means",m,mq
    print "stdd",N.sqrt(N.sum((x-m)**2*h)/N.sum(h))
    print "stdd",N.sqrt(N.sum((x-mq)**2*hq)/N.sum(hq))

    print "nspec",len(zspec)
    print "nspec sel",len(zspec_sel)

    pylab.savefig("svm_zhist_%s.pdf"%tag)

    pylab.figure()

    data_sel = data[ii]
    c1,c2,c3,c4,mi = data.T
    c1h,c2h,c3h,c4h,mih = data_sel.T

    labels = ['u-g','g-r','r-i','i-z','magi']
    for i in [0,1,2,3,4]:
        for j in [0,1,2,3,4]:
            if i>=j: continue
            pylab.subplot(5,5,5*(j)+i+1)
            pylab.plot(data.T[i],data.T[j],".",c='grey')
            pylab.plot(data_sel.T[i],data_sel.T[j],".",c='r')

            pylab.xlabel(labels[i])
            pylab.ylabel(labels[j])
            if i<4: pylab.xlim(-0.5, 2.5)
            if j<4: pylab.ylim(-2, 2)
            if j==4: pylab.ylim(19,22.9)


    pylab.savefig("svmsel_%s.png"%tag)
    #pylab.show()





if __name__=="__main__":
    if "submit" in sys.argv[1:]:
        os.system("sbatch -n1 -Jsvm --wrap=\"python -u %s %s %s\""%(sys.argv[0],sys.argv[2],sys.argv[3]))
        exit()
    main(zmin=float(sys.argv[1]),zmax=float(sys.argv[2]))
    
