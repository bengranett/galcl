import sys
import os
import numpy as N
import pylab

from sample import Sample
from galaxycl import GalaxyCl

import params

import time

def load(zbounds):
    """ """
    ra,dec,photz = N.load(params.catalog_path_cache).T[:3]
    sel = (photz>=zbounds[0])&(photz<zbounds[1])
    cat = N.transpose([ra[sel],dec[sel]])
    print "> loaded",len(cat)
    return cat

def determine_nbar(zbin, lockfile="nbar.lock"):
    """ """
    lockflag = True
    while os.path.exists(lockfile):
        print "> waiting for nbar to be computed by another process..."
        time.sleep(10)
        lockflag = False
    
    if lockflag:
        out = file(lockfile,"w")
        out.write("%s"%os.getpid())
        out.close()
    
    ngal = 0.
    area = 0.
    for field in params.fields.keys():
        mask_stuff = params.fields[field]

        zbounds = params.photz_bins[zbin:zbin+2]
    
        zdist = N.load('data/zdist/zdist.npy')
        z = zdist[0]
        n = zdist[1:]

        zdist = z,n[zbin]
        cat = load(zbounds)

        name = "%sz%i"%(field,zbin)

        S = Sample(name=name, catalog=cat, zdist=zdist, 
                   bias=1.4, starfrac=params.starfrac[field])
        S.add_angular_field_mask_file(*mask_stuff['field_mask'])
        S.add_angular_hole_mask_file(*mask_stuff['hole_mask'])
        S.get_nbar()
        ngal += S.nbar*S.area
        area += S.area

        print "> %s area: %g,  nbar: %g"%( field, S.area, S.nbar)

    nbar = ngal*1.0/area
    print "> tot area: %g, mean nbar: %g"%(area, nbar)

    if lockflag:
        os.remove(lockfile)

    return nbar

def load_sample(field, zbin, nbar):
    mask_stuff = params.fields[field]

    zbounds = params.photz_bins[zbin:zbin+2]
    
    zdist = N.load('data/zdist/zdist.npy')
    z = zdist[0]
    n = zdist[1:]
    zdist = z,n[zbin]

    cat = load(zbounds)

    name = "%sz%i"%(field,zbin)

    S = Sample(name=name, catalog=cat, zdist=zdist, 
               bias=1.4, starfrac=params.starfrac[field])
    S.add_angular_field_mask_file(*mask_stuff['field_mask'])
    S.add_angular_hole_mask_file(*mask_stuff['hole_mask'])

    S.make_healpix_map(fix_nbar=nbar)

    return S



def main(field='W3', zbin = 0):
    """ """
    mean_nbar = determine_nbar(zbin)

    S = load_sample(field,zbin,mean_nbar)
    G = GalaxyCl(S)

    G.quadest()
    
def submit_all_fields(zbin,fields=None):
    if fields==None:
        fields = params.fields.keys()
    for field in fields:
        name = "%s%s"%(field,zbin)
        cmd = "sbatch -n4 -J%s -eslurm_%s-%%j.err -oslurm_%s-%%j.out --mem=12GB --time=1\:00\:00 --wrap=\"python -u main.py %s %s\""%(name,name,name, field, zbin)
        print cmd
        os.system(cmd)
        time.sleep(1)

if __name__=="__main__":

    if "submit" in sys.argv[1]:
        if len(sys.argv)>3:
            fields = sys.argv[3:]
        else:
            fields = None
        submit_all_fields(sys.argv[2], fields)
        exit()

    field,zbin = sys.argv[1:]
    zbin = int(zbin)

    main(field,zbin)
