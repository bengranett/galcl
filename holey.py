""" Mask processor """
import sys
import numpy as N
from scipy.spatial import cKDTree
import time
from matplotlib.path import Path

import sphere


class holey:
    
    def __init__(self, regionfile=None):
        """ """
        self.mask = {}
        if not regionfile==None:
            self.loadregion(regionfile)


    def loadregion(self, path):
        tag = 'None'
        mask = {}
        linenu = 0
        for line in file(path):
            linenu += 1
            a = line.find("(")
            if a<0: continue
            b = line.find(")")
            if b<0: continue

            label = line[:a]
            label = label.lower()

            if label.endswith("circle"):
                tag = 'circle'
            elif label.endswith("box"):
                tag = 'box'
            elif label.endswith("polygon"):
                tag = 'polygon'
            else:
                print "unknown label!",label
                continue

            coords = line[a+1:b]
            coords = coords.replace(","," ")

            try:
                c = [float(v) for v in coords.split()]
            except:
                print "can't parse line %i"%linenu
                print line
                print coords
                raise
            if not mask.has_key(tag):
                mask[tag] = []

            mask[tag].append(c)
        self.mask = mask


    def getRadius(self, tag, c):
        """ """
        if tag=='circle':
            x,y,r = c
            return x,y,r
        elif tag=='box':
            x,y,dx,dy = c
            return x,y,N.sqrt(dx**2+dy**2)
        elif tag=='polygon':
            poly = c.reshape((len(c)//2,2))
            a = poly.min(axis=0)
            b = poly.max(axis=0)
            r = N.sqrt(N.sum((b-a)**2))/2
            x,y = (a+b)/2.

            return x,y,r
        
    def checkinside(self, tag, maskcoord, cc):
        """ """
        tx,ty = N.transpose(cc)
        if tag=='circle':
            x,y,r = maskcoord
            return sphere.distance((x,y),(tx,ty))<r
        elif tag=='box':
            x,y,dx,dy = maskcoord
            hx = dx/2./N.cos(y*N.pi/180)
            hy = dy/2.
            return N.logical_and(N.abs(tx-x)<hx, N.abs(ty-y)<hy)
        elif tag=='polygon':
            poly = maskcoord.reshape((len(maskcoord)//2,2))
            poly_path = Path(poly)
            return poly_path.contains_points(cc)
        else:
            print "oh hell"
            

    def contains(self,lon=None,lat=None):
        """ """
        t0 = time.time()
        
        if lon is not None:
            data = N.transpose((lon,lat))

            tree = cKDTree(data)
            coords = N.transpose((lon,lat))

            self.tree = tree
            self.coords = coords
        else:
            tree = self.tree
            coords = self.coords

        counter = 0
        inside = []
        for tag in ['circle','box','polygon']:
            if not self.mask.has_key(tag): continue
            
            for maskcoord in self.mask[tag]:
                maskcoord = N.array(maskcoord)
                counter += 1
                x,y,r = self.getRadius(tag, maskcoord)
                cos = N.cos(y*N.pi/180)
                                
                matches = tree.query_ball_point((x,y), r=r/cos*1.1)

                if len(matches)==0: continue
                matches = N.array(matches)
                cc = coords[matches,:]
                ii = self.checkinside(tag,maskcoord,cc)
                inside.append(matches[ii])

        inside = N.array(inside).flatten()

        s = N.zeros(len(coords))
        for i in inside:
            s[i] = 1

        #print "Holey time (n=%i, m=%i): %g"%(len(s),counter,time.time()-t0)

        return s>0

    # alias to contains function
    check = contains


def test(n=1e5):
    import pylab
    x = N.random.uniform(0,1,n)
    y = N.random.uniform(0,1,n)
    mask = {#'circle':[(.50,.50,.10),
            #          (.6,.8,.2)],
            'box':[(0.5,0.5,.3,.3)],
            'polygon':[(0.2,0.1,0.3,0.1,0.15,0.8)]}

    H = holey()
    H.mask = mask
    ii = H.check(x,y)==False

    print ii

    #pylab.plot(x,y,",")
    pylab.plot(x[ii],y[ii],",")
    pylab.show()


def test2(n=1e6):
    import pylab

    x = N.random.uniform(210,212,n)
    y = N.random.uniform(54,55,n)

    H = holey('data/merate_mask/photo_W3.reg')
    ii = H.check(x,y,density=5e7)==False

    pylab.plot(x[ii],y[ii],",")
    pylab.show()


def spheretest(n=1e7):
    import pylab
    lon = N.random.uniform(-180,180,n)
    z = N.random.uniform(0,1,n)
    lat = 90-N.arccos(z)*180/N.pi


    H = holey()


    i = 0
    print "go"
    for l in N.arange(5,100,20):
        i+=1
        mask = {'circle':[]}
        mask['circle'].append((170,l,2))

        H.mask = mask

        if i==1:
            ii = H.check(lon,lat)
        else:
            ii = H.check()
            
        pylab.plot(lon[ii],lat[ii],",")

        print l,len(lon[ii])


    for l in N.arange(5,100,20):
        mask = {'box':[]}
        mask['box'].append((175,l,2,2))

        H.mask = mask

        ii = H.check()

        pylab.plot(lon[ii],lat[ii],",")

        print l,len(lon[ii])

        
        
    pylab.show()
              

if __name__=="__main__":
    spheretest()
    #test2()