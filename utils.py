import os
import sys
import numpy as np
import collections

def run_or_die(cmd):
    """ Execute a shell command.  If the result code is not 0, exit.

    Arguments:
    cmd - command to run
    
    Return:
    None
    """
    r=os.system(cmd)
    if not r==0:
        print >>sys.stderr, cmd
        print >>sys.stderr, "Dead!"
        print >>sys.stderr, "-->  command: %s"%cmd
        print >>sys.stderr, "--->  result: %i"%r
        sys.exit(r)


def is_sequence(a):
    """ Return True if a variable is a sequence including numpy array. 

    Arguments:
    a - variable to check

    Return:
    bool - True if it is a sequence, False otherwise
    """

    # test if string
    if isinstance(a, (str,unicode)): return False

    # now test against sequence types
    if not isinstance(a, (collections.Sequence, np.ndarray)): 
        return False

    # could still be a scalar, eg numpy.array(1)
    try:
        l = a.__len__()
    except TypeError:
        return False
    except AttributeError:
        return False

    # ok it is probably a sequence of some sort
    return True

def is_sequence_test():
    print is_sequence("ab")==False
    print is_sequence(1)==False
    print is_sequence([1])==True
    print is_sequence((1,))==True
    print is_sequence(np.array(1))==False
    print is_sequence(np.array([1,2,3]))==True


if __name__=="__main__":
    pass
