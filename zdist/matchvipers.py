import sys,os
sys.path.append(".")
import cPickle as pickle

import pylab
import numpy as N

from sklearn.neighbors import KDTree

import params

import torndb as database
db=database.Connection("frontend", "vipers",user="",password="")

def loadssr():
    """ """
    lookup = {}
    for line in file("data/cats/ssr_all_v7.txt"):
        if line.startswith("#"): continue
        num,a,b,c = line.split()
        lookup[num] = float(a)

    return lookup
    

def loadvipers(savefile="data/cats/vipers.npy"):
    """ """
    if os.path.exists(savefile):
        return N.load(savefile)

    ssr = loadssr()

    query = """select S.num, S.zspec, S.zflg, S.alpha,S.delta
from  SPECTRO_V7_0 as S
join PHOT as P on S.num=P.num
where P.vmmpsflag='S'
  and P.newflag=1
  and ((zflg >= 2 and zflg < 10) or (zflg >= 12 and zflg < 20))
    
and S.zspec between -.01 and 5;"""

    results = db.query(query)

    flags = []
    data = []
    for r in results:
        num = r['num']
        ra = r['alpha']
        dec = r['delta']
        zspec = r['zspec']
        zflg = r['zflg']
        
        flags.append(zflg)

        if ssr.has_key(num):
            s = ssr[num]
        else:
            print "no ssr! ",num,zspec,zflg
            s = 0

        data.append((float(num),ra,dec,zspec,s))

    print "flags selected:"
    print N.unique(flags)

    data = N.array(data)

    N.save(savefile, data)

    return data


def load_vvds_wide():
    """ vvds wide """
    data = []
    for field in ["F22"]:
        path = "vvds_wide/cesam_vvds_sp%s_WIDE.txt"%field
        for line in file(path):
            if line.startswith("#"): continue
            w = line.split()
            ra = float(w[2])
            dec = float(w[3])
            #flag = int(w[6])
            #zspec = float(w[5])
            data.append((ra,dec,0))
    return N.array(data)


def distance(a,b):
    """ """
    r2 = N.sum((a-b)**2,axis=0)
    return N.sqrt(r2)

def match(ref, cat, r=1./3600, treepath="data/cats/cattree.pickle"):
    """ """    
    print "> planting tree..."
    tree = KDTree(ref[:,:2])
    print "> done"


    m = tree.query_radius(cat[:,1:3], r=r)

    pickle.dump(m, file("data/cats/matches.pickle","w"))

    zspec = N.zeros(len(ref)) - 1  # default to -1
    zphot = N.zeros(len(cat)) - 1  # default to -1
    mags = N.zeros((len(cat),5))

    i = 0
    nmcount = 0
    for i,ii in enumerate(m):
        if len(ii)==0:
            nmcount += 1
        if len(ii)==1:
            zspec[ii[0]] = cat[i,3]
            zphot[i] = ref[ii[0]][2]
            mags[i] = ref[ii[0]][3:]
        if len(ii)>1:
            print i,cat[i]
            s = []
            for j in ii:
                r = 3600*distance(cat[i][1:3],ref[j][:2])
                imag = ref[j][6]
                print "~~~   ",j,r,imag
                s.append(imag)
            sel = N.argmin(s)
            print "      ***",sel
            zspec[ii[sel]] = cat[i,3]
            zphot[i] = ref[ii[sel]][2]
            mags[i] = ref[ii[sel]][3:]


    N.save("data/cats/phot_zspec.npy", zspec)
    N.save("data/cats/spec_zphot.npy", zphot)
    N.save("data/cats/phot_mags.npy", mags)

    cat = N.vstack([cat.T,zphot]).T
    N.save(params.catalog_spec_path, cat)

    print "no matches",nmcount


def merge():
    vcat = N.load('data/cats/vipers_cat.npy')
    photz = N.load('data/cats/spec_zphot.npy')
    print vcat.shape,photz.shape
    cat = N.vstack([vcat.T,photz]).T
    print cat.shape

    N.save(params.catalog_spec_path, cat)


def main():
    vcat = loadvipers()

    pcat = N.load(params.catalog_path_cache)

    #pylab.plot(*pcat.T[:2],marker=",",c='k',ls="None",markevery=10)
    #pylab.plot(*vcat.T[:2],marker="o",c='r',ls="None",markevery=10,mec='None',mfc='y')
    #pylab.plot(*wcat.T[:2],marker="o",c='y',ls="None",mec='None',mfc='y')

    match(pcat, vcat)



    #pylab.show()

main()
