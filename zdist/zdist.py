import sys
sys.path.append(".")
import pylab
import numpy as N

from scipy.ndimage import gaussian_filter1d
from sklearn import mixture
import scipy
import scipy.optimize

from matplotlib.ticker import MaxNLocator,MultipleLocator,AutoMinorLocator


import params


def gaussian(params, x):
    """ """
    m = len(params)
    n = m//3
    mu = params[0::3]
    sig = params[1::3]
    amp = params[2::3]

    y = N.zeros(len(x),dtype='d')
    for i in range(n):
        y += N.abs(amp[i])*1./N.sqrt(2*N.pi)*1./N.abs(sig[i])*N.exp(-(x-mu[i])**2/2./sig[i]**2)
        
    return y

def resid(params, x, y, yerr):

    mu = params[0::3]
    sig = params[1::3]
    amp = params[2::3]

    penalty = 0

    #print mu
    norm =  len(x)*yerr.min()
    penalty += norm*N.abs(N.sum(((mu-0.5)/2.)**4))

    dsig = N.log10(N.abs(sig))-N.log10(0.2)
    penalty += norm*N.abs(N.sum((dsig/1)**4))

    #print "penal",penalty

    chi2 = N.sum((y - gaussian(params,x))/yerr)
    #print "chi2",chi2,penalty

    return (y - gaussian(params,x))/yerr + penalty
        

def fit(x, y, yerr, n_components=3):
    """ """
    dist = N.cumsum(y)*1./N.sum(y)
    a = x[dist.searchsorted(0.1)]
    b = x[dist.searchsorted(0.9)]
        
    #print a,b

    mu = N.linspace(a,b, n_components)
    sig = N.ones(n_components)*N.std(x)/n_components
    amp = N.ones(n_components)*1./n_components

    p0 = []
    for i in range(n_components):
        p0.append([mu[i],sig[i],amp[i]])
    p0 = N.concatenate(p0)

    #print p0


    f, info = scipy.optimize.leastsq(resid, p0, (x, y, yerr))
    print "mu ",f[0::3]
    print "sig",f[1::3]
    print "amp",f[2::3]

    print "info",info
    return f

def load():
    """ """
    cat = N.load(params.catalog_spec_path)

    cat = cat.T

    zspec = cat[3]
    zphot = cat[5]

    ii = zphot>-.5

    zspec = zspec[ii]
    zphot = zphot[ii]
    ssr = cat[4][ii]

    ii = ssr==0
    print "missing ssr",N.sum(ii),len(ssr)

    ssr[ssr==0] = 1.

    return zspec, zphot, ssr


def zzplot(zspec,zphot):
    """ """
    pylab.subplot(aspect='equal')
    pylab.plot(zspec,zphot,",")
    zmax = zphot.max()
    pylab.plot([0,zmax],[0,zmax])
    pylab.show()


def zbins(zspec,zphot,weight):
    pylab.figure(figsize=(4,8))
    zhistbins = N.arange(0.1,2,.01)
    cent = (zhistbins[1:] + zhistbins[:-1]) / 2.

    zdist = [cent]

    stats = []

    bins = params.photz_bins
    nplot = len(bins)-1
    for i in range(len(bins)-1):
        sel = (zphot>=bins[i])&(zphot<bins[i+1])

        print N.mean(weight[sel])
        
        nstar = N.sum(N.abs(zspec[sel])<.01)
        starfrac = nstar*1./N.sum(sel)

        h,e = N.histogram(zspec[sel], zhistbins)
        hw,e = N.histogram(zspec[sel], zhistbins,weights=weight[sel])

        #h = N.ma.array(h, mask=h==0)
        #hw = N.ma.array(hw, mask=hw==0)

        err = N.sqrt(h)
        err[err<1] = 1

        norm = N.sum(hw)*(cent[1]-cent[0])

        hw = hw*1./norm
        err = err*1./norm
        
        
        f = fit(cent,hw,err)
        yfittot = gaussian(f, cent)

        zdist.append(yfittot)

        yfit = []
        for j in range(len(f)//3):
            yfit.append(gaussian(f[j*3:(j+1)*3], cent))


        mu = N.mean(zspec[sel])
        sig = N.std(zspec[sel])
        print bins[i],bins[i+1],N.sum(sel),N.mean(zspec[sel]),N.std(zspec[sel])

        subploti = i+1
        #if i<5:
        #    subploti = i*2+1
        #else:
        #    subploti = (i-5)*2+2
        ax = pylab.subplot(nplot,1,subploti)
        #for j in range(len(yfit)):
        #    pylab.plot(cent,yfit[j],dashes=[5,3],c='firebrick')

        pylab.plot(cent,yfittot,dashes=[4,1],c='firebrick')

        #pylab.plot(cent,h,dashes=[4,1],c='dodgerblue')
        pylab.plot(cent,hw,c='k')

        pylab.axvline(mu,c='navy',lw=0.5)

        pylab.text(0.65,0.85,"zphot [%3.2f,%3.2f]"%(bins[i],bins[i+1]),transform=ax.transAxes)
        pylab.text(0.65,0.75,"N = %i"%N.sum(sel),transform=ax.transAxes)
        pylab.text(0.65,0.65,"mean z = %4.3f"%mu,transform=ax.transAxes)
        pylab.text(0.65,0.55,"stddev z = %3.2f"%sig,transform=ax.transAxes)
        pylab.text(0.65,0.45,"starfrac = %3.3f"%starfrac,transform=ax.transAxes)

        stats.append((bins[i],bins[i+1],N.sum(sel), mu, sig, starfrac))
        

        pylab.fill_between([bins[i],bins[i+1]],[10,10],1e-10,facecolor='lightgrey',edgecolor='None',zorder=0)

        pylab.ylim(0,9)
        pylab.xlim(0.3,1.5)

        ax.xaxis.set_minor_locator(MultipleLocator(0.1))
        ax.xaxis.set_major_locator(MultipleLocator(0.2))

        ax.yaxis.set_minor_locator(MultipleLocator(1))
        ax.yaxis.set_major_locator(MultipleLocator(5))

        if subploti<len(bins)-2:
            pylab.setp(ax.get_xticklabels(), visible=False)
        else:
            pylab.xlabel("Spectroscopic redshift")
        #if not subploti%2:
        #    pylab.setp(ax.get_yticklabels(), visible=False)
        if subploti==3:
            pylab.ylabel("Normalized N(z)")

    #N.save("data/zdist/stats.npy",stats)
    #N.save("data/zdist/zdist.npy",zdist)

    pylab.subplots_adjust(left=0.1,bottom=0.05,top=0.98,right=.98,hspace=0,wspace=0)
    #pylab.tight_layout()
    
    
    pylab.savefig("plots/zdist.pdf")

    pylab.show()


def zdistall(zspec,zphot,weight):
    """ """
    pylab.figure(figsize=(4,8))
    zhistbins = N.arange(0.1,2,.02)
    cent = (zhistbins[1:] + zhistbins[:-1]) / 2.
    
    nstar = N.sum(N.abs(zspec)<0.01)
    starfrac = nstar*1./len(zspec)
    
    h,e = N.histogram(zspec, zhistbins)
    hw,e = N.histogram(zspec, zhistbins,weights=weight)
    
    err = N.sqrt(h)
    err[err<1] = 1

    norm = N.sum(hw)*(cent[1]-cent[0])
    normh = N.sum(h)*(cent[1]-cent[0])

    print "norm",norm,normh

    hw = hw*1./norm
    h = h*1./normh
    err = err*1./norm
        
        
    f = fit(cent,hw,err,n_components=4)
    yfittot = gaussian(f, cent)

    f2 = fit(cent,h,err,n_components=4)
    yfittot2 = gaussian(f2, cent)


    yfit = []
    for j in range(len(f)//3):
        yfit.append(gaussian(f[j*3:(j+1)*3], cent))


    mu = N.mean(zspec)
    sig = N.std(zspec)

    pylab.plot(cent,yfittot,dashes=[4,1],c='firebrick')
    pylab.plot(cent,yfittot2,dashes=[4,1],c='navy')

    pylab.semilogy(cent,h,dashes=[4,1],c='dodgerblue')
    pylab.semilogy(cent,hw,c='k')

    pylab.axvline(mu,c='navy',lw=0.5)
    ax = pylab.gca()

    pylab.text(0.65,0.75,"N = %i"%len(zspec),transform=ax.transAxes)
    pylab.text(0.65,0.65,"mean z = %4.3f"%mu,transform=ax.transAxes)
    pylab.text(0.65,0.55,"stddev z = %3.2f"%sig,transform=ax.transAxes)
    pylab.text(0.65,0.45,"starfrac = %3.3f"%starfrac,transform=ax.transAxes)


        

    pylab.ylim(1e-2,3)
    pylab.xlim(0.3,1.5)


    ax.xaxis.set_minor_locator(MultipleLocator(0.1))
    ax.xaxis.set_major_locator(MultipleLocator(0.2))

    ax.yaxis.set_minor_locator(MultipleLocator(1))
    ax.yaxis.set_major_locator(MultipleLocator(5))

    pylab.xlabel("Spectroscopic redshift")
    
    pylab.ylabel("Normalized N(z)")

    pylab.subplots_adjust(left=0.05,bottom=0.05,top=0.98,right=.98,hspace=0,wspace=0)
    #pylab.tight_layout()

    
    pylab.savefig("zdist_all.pdf")
    pylab.show()

zspec,zphot,ssr = load()

#zzplot(zspec,zphot)

weight = 1./ssr


#weight /= N.sum(weight)
#print N.sum(weight)


#zdistall(zspec,zphot,weight)
zbins(zspec,zphot,weight)
