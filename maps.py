
import numpy as N
import healpy
import cPickle as pickle

def healpixit(lon,lat,nside,nest=True):
    """ """
    phi = N.pi/180*lon
    theta = N.pi/180*(90-lat)
    p = healpy.ang2pix(nside,theta,phi,nest=nest)

    m = N.bincount(p)
    map = N.zeros(12*nside**2)
    map[:len(m)] = m

    return map



def dump_map(map, mask, filename):
    """ """
    npix = len(map)
    ii = mask>0
    pix = N.arange(npix)[ii]
    pickle.dump((npix,pix,map[ii]),file(filename,'w'))

def read_map(filename):
    """ """
    npix,pix,mapflat = pickle.load(file(filename))

    map = N.zeros(npix,dtype='d')
    map[pix] = mapflat

    return map
