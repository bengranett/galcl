import sys
import os
import numpy as N
import healpy
import clib
import time
#import pylab


class Cache:
    cache = {}
    requests = []

    def __init__(self, memlimit = 24):
        self.memlimit = memlimit

    def insert(self, key, value):
        self.cache[key] = value
        self.requests.insert(0,key)
        #print "insert",key
        print "cache request list",self.requests

    def get(self, key):
        #print "get",key
        try:
            i = self.requests.index(key)
            self.requests.pop(i)
        except ValueError:
            pass
        self.requests.insert(0,key)

        print "cache request list",self.requests
        return self.cache[key]

    def has_key(self, key):
        return self.cache.has_key(key)

    def getsize(self):
        m = 0
        for key in self.cache.keys():
            m += self.cache[key].nbytes
        a = m/1024.**3
        print "size",a
        return a

    def clean(self):
        #print "cleaning cache!"
        while self.getsize()>self.memlimit:
            block = self.requests.pop()
            print "del",block
            del self.cache[block]

    def delete(self, key):
        #print "deleting key",key
        try:
            i = self.requests.index(key)
            self.requests.pop(i)
        except ValueError:
            pass
        try:
            del self.cache[key]
        except:
            pass

    def done(self):
        #print "deleting cache"
        #del self.cache
        self.cache = {}
        self.requests = []
        
    

def getDmatblock(block,ll, Cinv, costhij, weight, cachefile=None,cache=None):
    """ """
    if cache.has_key(block):
        return cache.get(block)

    if cachefile != None:
        cachefile = cachefile%block
        if os.path.exists(cachefile):
            print "> reading from disk",cachefile
            d = N.load(cachefile)
            cache.insert(block, d)
            return d

    
    t0 = time.time()
    print "> computing Dmats l=%i..%i"%(ll[0],ll[-1]-1),
    sys.stdout.flush()
    
    d = clib.legP_vec_parallel(ll, costhij, weight)
    print time.time()-t0,
    sys.stdout.flush()

    t1 = time.time()
    for i in range(len(d)):
        d[i] = N.dot(Cinv,d[i])
    
    print time.time()-t1
    cache.insert(block,d)

    if cachefile!=None:
        print "> writing to disk %s"%cachefile
        N.save(cachefile, d)
    
    #cache_index.append(block)
    return d
  

def fisher(Cinv, bands, mask, weight=None, nside=512, nest=True, memory=5,
           tag='ciao', output='cache/fisher.pickle'):
    """
    Compute the fisher matrix.  The computation is done in blocks
    so that only a few derivative matrices are stored in memory.
    """
    # load cache file if available
    if os.path.exists(output):
        return N.load(output)

    blocksize = min(20, int(1*1024**3*1./Cinv.nbytes))
    cachefile = None#'cache/dmat/dmat_'+tag+'_%i.npy'

    assert(blocksize*Cinv.nbytes*1./1024**3 < memory)

    print "memory use",memory
    cache = Cache(memory)


    #ll = N.arange(lmin,lmax,step)
    ll = bands
    
    ii = N.where(mask==1)
    s = N.arange(12*nside**2)[ii]
    v = healpy.pix2vec(nside, s, nest=nest)

    costhij = N.multiply.outer(v[0],v[0])+ \
              N.multiply.outer(v[1],v[1])+N.multiply.outer(v[2],v[2])

    if weight is None:
        weight = N.ones(bands[-1])


    n = len(ll)-1
    fish = N.zeros((n,n))

    # how many dmatrices can be fit in memory (2GB)
    #blocksize = min(n, int(memory*1024**3*1./Cinv.nbytes))
    print 'blocksize is', blocksize

    nb = int(N.ceil(n*1./blocksize))
    print "number of blocks",nb

    print "starting block fisher matrix"
    for b1 in range(nb):
        ll1 = ll[b1*blocksize:(b1+1)*blocksize+1]
        m1 = len(ll1)-1
    
        
        for b2 in range(b1,nb):
            if b1%2:
                b2 = nb-b2+b1-1

            print b1,b2

            
            # skip far off the diagonal
            #if N.abs(b1-b2)*blocksize > 200: continue
            
            ll2 = ll[b2*blocksize:(b2+1)*blocksize+1]
            m2 = len(ll2)-1


            d1 = getDmatblock(b1,ll1, Cinv, costhij, weight,
                              cachefile=cachefile, cache=cache)

            d2 = getDmatblock(b2,ll2, Cinv, costhij, weight,
                              cachefile=cachefile, cache=cache)
            if b2!=b1:
                #cache_remove(b2)
                cache.clean()

            block = N.zeros((m1,m2))
            for i1 in range(m1):
                for i2 in range(m2):
                    if b1==b2:
                        if i2>i1: continue
                        block[i1,i2] = (d1[i1]*d2[i2].transpose()).sum()
                        if i2!=i1: block[i2,i1] = block[i1,i2]
                    else:
                        block[i1,i2] = (d1[i1]*d2[i2].transpose()).sum()

            # remember to divide by 2
            block *= 0.5
            
            fish[b1*blocksize:(b1+1)*blocksize, b2*blocksize:(b2+1)*blocksize] = block
            if b1!=b2:
                fish[b2*blocksize:(b2+1)*blocksize, b1*blocksize:(b1+1)*blocksize] = block.transpose()

                
        N.save(output,fish)
        cache.delete(b1)  # this block is no longer needed since the row is finished
        cache.clean()

    cache.done()
    cache = None  # clear the cache

    N.save(output,fish)
    print "done"
    return fish
    
