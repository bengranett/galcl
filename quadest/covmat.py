import sys,os
sys.path.append("..")
sys.path.append(".")
import pylab
import healpy
import numpy as N

import theorycl
import cPickle as pickle
import clib
import scipy
from scipy.interpolate import interp1d
import bins
import time
import inv
import dmat
import fisher as gofish
import healpixtools


def cf(cl, thetamax=180,n=1e5, label='ciao', cache='cache/xigrid_%s.pickle'):
    """ Compute the correlation function from Cl. """
    tag = '%s_%i_%i'%(label,thetamax*10,N.log10(n)*10)

    xi = None
    if not cache==None:
        cache = cache%tag
        print "cache file:",cache
        if os.path.exists(cache):
            print "loading xi from file",cache
            mu,xi = pickle.load(file(cache))
            
    if xi == None:
        mumin = N.cos(N.pi/180*thetamax)
        mu = N.linspace(mumin,1,n)
        print "computing xi at",len(mu)
        t0 = time.time()

        xi = clib.xiFromCl(mu, cl)
        print "done",time.time()-t0
        if not cache==None:
            print "writing xi to file",cache
            pickle.dump((mu,xi),file(cache,'w'))

    t1 = time.time()
    interp = interp1d(mu,xi,"linear")

    print "interpolation time:",time.time()-t1

    return interp


def covmat(cl, mask, label='yo', noise=None, bunchsize=10000,
           cache='cache/cmat_%s.npy',
           cacheinv='cache/cmatinv_%s.npy',
           nest=True):
    """
    Compute covariance matrix

    Inputs
    -------

    Outputs
    -------

    """
    nside = healpy.npix2nside(len(mask))

    tag = "%s_%i_%i"%(label,N.sum(mask),nside)

    cache = cache%tag
    if cacheinv is not None:
        cacheinv = cacheinv%tag
        if os.path.exists(cacheinv):
            print "> loading cmat inv from file:",cacheinv
            return N.load(cacheinv)

    
    costhij,thetamax = healpixtools.compute_separations(mask, nest=nest)
    shape = costhij.shape
    print "> Cov mat shape",shape
    costhij = costhij.flatten()

    
    t0 = time.time()
    print "> computing xi with thetamax=",thetamax
    xi = cf(cl, thetamax,label=label)

    print "> interpolating xi to",len(costhij)

    covmat = []
    for i in range(0,len(costhij),bunchsize):
        covmat.append(xi(costhij[i:i+bunchsize]))
    covmat = N.hstack(covmat)
    print "done, time:",time.time()-t0

    
    covmat = N.reshape(covmat, shape)

    # add shot noise component
    if noise is not None:
        covmat += N.eye(len(covmat)) * noise[mask>0]

    N.save(cache,covmat)
    print "> wrote cmat to file",cache

    t0 = time.time()

    print "> cov mat inverse"
    covmatinv = inv.pinv(covmat, cut=0)
    print "> done",time.time()-t0

    if cacheinv is not None:
        N.save(cacheinv, covmatinv)
    return covmatinv
