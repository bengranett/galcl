import pylab
import numpy as N
import clib
import healpy
import sys,os
import time
import cPickle as pickle
import scipy
import healpixtools

def dmat_fast(bands, mask, weight=None, cache='cache/dmat_%s.pickle', nest=True):
    nside = healpy.npix2nside(len(mask))
    tag = "%i%i%i_%i_%i_%s"%(len(bands),bands[0],bands[-1],N.sum(mask),nside,"%i")
    if cache is not None:
        cache = cache%tag

    n = len(N.where(mask>0)[0])

    if cache:
        print "loading dmats from huge files"
        for i in range(len(bands)-1):
            print i
            if not os.path.exists(cache%i): break
            dmats = N.zeros((len(bands)-1,n,n),dtype='d')
            dmats[i,:,:] = pickle.load(file(cache%i))
        if i == len(bands)-2:
            print "done"
            return dmats

    t0 = time.time()

    print "computing dmat"
    costhij,thetamax = healpixtools.compute_separations(mask, nest=nest)


    t0 = time.time()

    if weight is None:
        weight = N.ones(bands[-1])


    print bands
    dmats = clib.legP_vec_parallel(bands,costhij,weight)

    assert(N.all(N.isfinite(dmats)))

    print "dmat size %g GB"%(dmats.nbytes/(1024.)**3),dmats.shape

    print "done",time.time()-t0
    if cache:
        for i in range(len(bands)-1):
            print "writing dmat pickle",i
            fid = file(cache%i,'w')
            pickle.dump(dmats[i],fid)
            fid.close()

    return dmats


def binkernel(k,kbands,kernel,lmax=1000):
    """
    bin the weight matrix in the k-bands
    resample the weight matrix into bins that fit nicely into the k-bands
    """
    kstep = k[1]-k[0]
    kbstep = kbands[1]-kbands[0]
    step = kbstep / int((kbstep/kstep)) / 10.

    print "kstep",kstep
    print "kbstep",kbstep
    print "kbands",kbands
    print "step",step

    scalefact = kstep*1./step
    print "scale fact",scalefact
    knew = N.arange(kbands[0],kbands[-1]+step,step)
    kern = []
    for w in kernel:
        inter = scipy.interpolate.interp1d(k,w,kind='linear')
        y = inter(knew)
        kern.append(y)

    kernel = N.array(kern)
    k = knew

    weight = N.zeros((lmax, len(kbands)-1))
    for i in range(len(kbands)-1):
        ii = N.where(k >= kbands[i])[0]
        jj = N.where(k[ii] < kbands[i+1])
        ii = ii[jj]
        weight[:,i] = N.sum(kernel[:lmax,ii],axis=1)/scalefact#*1./len(ii)
    return weight


def dmat_fast3d(kbands, mask, k, kernel, lmax=1000, cache='cache/dmat_%s.pickle', nest=True):
    nside = healpy.npix2nside(len(mask))
    tag = "%i%i%i_%i_%i_%s"%(len(kbands),kbands[0]*100,kbands[-1]*100,N.sum(mask),nside,"%i")
    if cache is not None:
        cache = cache%tag

    n = len(N.where(mask>0)[0])

    if cache:
        print "loading dmats from huge files"
        for i in range(len(bands)-1):
            print i
            if not os.path.exists(cache%i): break
            dmats = N.zeros((len(bands)-1,n,n),dtype='d')
            dmats[i,:,:] = pickle.load(file(cache%i))
        if i == len(bands)-2:
            print "done"
            return dmats

    t0 = time.time()

    print "computing dmat"
    costhij,thetamax = healpixtools.compute_separations(mask, nest=nest)

    t0 = time.time()

    # bin the weight matrix in the k-bands
    weight = binkernel(k,kbands,kernel,lmax=lmax)


    dmats = clib.legP_3d_parallel(kbands,costhij,weight,lmax)

    assert(N.all(N.isfinite(dmats)))

    print "dmat size %g GB"%(dmats.nbytes/(1024.)**3),dmats.shape

    print "done",time.time()-t0
    if cache:
        for i in range(len(bands)-1):
            print "writing dmat pickle",i
            fid = file(cache%i,'w')
            pickle.dump(dmats[i],fid)
            fid.close()

    return dmats


def dmat(bands, mask, cache='cache/dmat_%s.pickle', nest=True):
    nside = healpy.npix2nside(len(mask))
    tag = "%i%i%i_%i_%i_%s"%(len(bands),bands[0],bands[-1],N.sum(mask),nside,"%i")

    n = len(N.where(mask>0)[0])
    dmats = N.zeros((len(bands)-1,n,n),dtype='d')

    print dmats.nbytes
    #sys.exit()

    if cache:
        cache = cache%tag

        print "loading dmats from huge files"
        for i in range(len(bands)-1):
            print i
            if not os.path.exists(cache%i): break
            dmats[i,:,:] = pickle.load(file(cache%i))
        if i == len(bands)-2:
            print "done"
            return dmats


    print "computing dmat"
    costhij,thetamax = healpixtools.compute_separations(mask, nest=nest)


    #n = len(v[0])
    #dmats = N.zeros((len(bands)-1,n,n),dtype='d')

    norm =  N.fromfunction(lambda l: (2*l+1)/4./N.pi,(bands[-1],))

    print "dmat size %g GB"%(dmats.nbytes/(1024.)**3)
    pp = scipy.zeros(bands[-1],'d')

    for i in range(n):
        print i,n,
        sys.stdout.flush()
	for j in range(i,n):
            pp = clib.legP(pp,int(bands[-1]),float(costhij[i,j]))
	    pp = pp*norm
	    dmats[:,i,j] =  \
	    N.array([N.add.reduce(pp[b1:b2]) for b1,b2 in zip(bands[:-1],bands[1:])])
	    dmats[:,j,i] = dmats[:,i,j]



    print "done"
    if cache:
        for i in range(len(bands)-1):
            fid = file(cache%i,'w')
            pickle.dump(dmats[i],fid)
            fid.close()

    return dmats

def dmat_interp(bands, mask, cache='cache/dmat_%s.pickle',ninterp=5e2, nest=True):
    
    nside = healpy.npix2nside(len(mask))
    tag = "%i%i%i_%i_%i_%s"%(len(bands),bands[0],bands[-1],N.sum(mask),nside,"%i")
    cache = cache%tag

    n = len(N.where(mask>0)[0])
    dmats = N.zeros((len(bands)-1,n,n),dtype='d')
    print "computing dmat"

    
    costhij,thetamax = healpixtools.compute_separations(mask, nest=nest)

    mu = N.linspace(costhij.min(),costhij.max(),ninterp)
    mu = N.linspace(-1,1,ninterp)

    lmax = int(bands[-1])
    pp = clib.legP_vec(lmax, mu)

def test(nside=128):
    bands = N.arange(50,200,50)

    i = N.arange(12*nside**2)
    lat,lon = healpy.pix2ang(nside,i,nest=True)
    lon *= 180/N.pi
    lat *= 180/N.pi

    ii = N.where(lon<8)[0]
    jj = N.where(N.abs(90-lat[ii])<4)

    mask = N.zeros(12*nside**2)
    mask[ii[jj]] = 1

    d1 = dmat_fast(bands,mask,cache=None)
    d2 = dmat(bands,mask,cache=None)

    print d1.shape
    print d2.shape

    pylab.figure()
    pylab.title("fast")
    pylab.imshow(N.log10(N.abs(d1[0])))
    pylab.colorbar()
    pylab.figure()
    pylab.title("orig")
    pylab.imshow(N.log10(N.abs(d2[0])))
    pylab.colorbar()

    pylab.figure()
    pylab.title("delta")
    pylab.imshow(N.log10(N.abs(d2[0]-d1[0])))
    pylab.colorbar()

    pylab.show()
    assert(N.allclose(d1,d2))

if __name__=="__main__":
    test()
