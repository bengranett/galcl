"""
     Some C-code to be used with python in mlhood

     (c) Istvan Szapudi, Hawaii, 2007

"""

import numpy as N
import scipy
import scipy.weave
import scipy.weave.inline_tools
import scipy.weave.c_spec
from scipy.weave.converters import blitz as cblitz

def legP(pl,lmax,amu):
    """
      calculate a vector containing
      p_0(amu),p_1(amu),...,p_lmax-1(amu)
      where p_l is a Legendre polynomial, and amu is a cos angle
    """
    
    #print type(lmax)
    #print type(amu)
    
    #print lmax,amu
    try:
        assert(type(lmax) == type(1))
    except:
        print lmax
        print type(lmax),type(1)
        raise
    assert(type(amu) == type(1.0))
    #assert(type(amu) == type(N.array([1.0])[0]) or type(1.0))                                            
    #pl = scipy.zeros((lmax),'d')
    code =\
    """                                                                   
    int l;
    double dl;
    pl(0)=1.;
    pl(1)=amu;
    for (l=2; l < lmax; l++){
      dl=(double) l;
      pl(l)=((2*dl-1)*amu*pl(l-1)-(dl-1)*pl(l-2))/dl;
    }
    """ 
    scipy.weave.inline(code,['pl','lmax','amu'],
                 type_converters = cblitz,
                 compiler='gcc')
    return pl

def legP_vec(bands,mu,weight):
    """
      calculate a vector containing
      p_0(amu),p_1(amu),...,p_lmax-1(amu)
      where p_l is a Legendre polynomial, and amu is a cos angle
    """
    
    #print type(lmax)
    #print type(amu)
    
    #print lmax,amu
    #assert(type(amu) == type(1.0))
    #assert(type(amu) == type(N.array([1.0])[0]) or type(1.0))

    lmax = int(bands[-1])
    s = mu.shape
    
    pl = scipy.zeros(bands[-1],'d')
    dmat = scipy.zeros((len(bands)-1, s[0], s[1]),'d')
    code =\
    """                                                                   
    int l,i,j,b,foundbin;
    double dl,tmp;
    b = 0;

    // printf(">>mu shape %i %i\\n",Nmu[0],Nmu[1]);
    // loop over 2-dim array mu
    for (i=0; i < Nmu[0]; i++){
      for (j=i; j < Nmu[1]; j++){
        // compute pl at this mu
        b = 0;
        pl(0)=1.;
        pl(1)=mu(i,j);
        for (l=2; l < lmax; l++){
          dl=(double) l;
          pl(l)=((2*dl-1)*mu(i,j)*pl(l-1)-(dl-1)*pl(l-2))/dl;

          // add to proper bin
          if (l >= bands(b)){
            foundbin = 0;
            while (foundbin == 0){
              if (b+1 >= Nbands[0]){
                break;
                }

              if (l < bands(b+1)){
                foundbin = 1;
              } else {
                b += 1;
              }
            }

            if (foundbin == 1){
              tmp = pl(l)*(2*l+1)*weight(l);
              dmat(b,i,j) += tmp;
              if (i!=j){dmat(b,j,i) += tmp;}
             }
          }
        }
      
     }
    }
    """ 
    scipy.weave.inline(code,['dmat','bands','pl','lmax','mu','weight'],
                 type_converters = cblitz,
                 compiler='gcc')
    return dmat/(4*N.pi)

def legP_vec_parallel(bands,mu,weight):
    """
      calculate a vector containing
      p_0(amu),p_1(amu),...,p_lmax-1(amu)
      where p_l is a Legendre polynomial, and amu is a cos angle
    """
    
    #print type(lmax)
    #print type(amu)
    
    #print lmax,amu
    #assert(type(amu) == type(1.0))
    #assert(type(amu) == type(N.array([1.0])[0]) or type(1.0))

    lmax = int(bands[-1])
    s = mu.shape
    
    #pl = scipy.zeros(bands[-1],'d')
    dmat = scipy.zeros((len(bands)-1, s[0], s[1]),'d')
    code =\
    """                                                                   
    int l,i,j,b,foundbin;
    int status,tid,nthreads;
    double dl,tmp,pl[10000];
    b = 0;
    nthreads = 0;
    status = 0;

    // printf(">>mu shape %i %i\\n",Nmu[0],Nmu[1]);
    // loop over 2-dim array mu
    #pragma omp parallel for private(i,j,pl,l,b,foundbin,dl,tmp,tid)
    for (i=0; i < Nmu[0]; i++){
      tid = omp_get_thread_num();
      if (tid == 0) {nthreads = omp_get_num_threads();}
      
      status += 1;
      fprintf(stderr,"\\rComputing deriv matrix: %i/%i (thread %i of %i)",status,Nmu[0],tid,nthreads);

    
      for (j=i; j < Nmu[1]; j++){
        // compute pl at this mu
        b = 0;
        pl[0]=1.;
        pl[1]=mu(i,j);
        for (l=2; l < lmax; l++){
          dl=(double) l;
          pl[l]=((2*dl-1)*mu(i,j)*pl[l-1]-(dl-1)*pl[l-2])/dl;

          // add to proper bin
          if (l >= bands(b)){
            foundbin = 0;
            while (foundbin == 0){
              if (b+1 >= Nbands[0]){
                break;
                }

              if (l < bands(b+1)){
                foundbin = 1;
              } else {
                b += 1;
              }
            }

            if (foundbin == 1){
              tmp = pl[l]*(2*l+1)*weight(l);
              dmat(b,i,j) += tmp;
              if (i!=j){dmat(b,j,i) += tmp;}
             }
          }
        }
      
     }
    }
    """ 
    scipy.weave.inline(code,['dmat','bands','lmax','mu','weight'],
                       extra_compile_args =['-O3 -fopenmp'],
                       extra_link_args=['-lgomp'],
                       headers=['<omp.h>'],
                       type_converters = cblitz,
                       compiler='gcc')
    return dmat/(4*N.pi)




def xiFromCl(mus, cl):
    """Calculate the correlation function from Cl's"""

    mus = scipy.array(mus, dtype='d')
    xi = scipy.zeros(mus.size,dtype='d')
    cl = scipy.array(cl, dtype='d')
    lmax =  len(cl)
    nmus = len(mus)
    
    code = """
    int l,i;                                                                
    double dl,pla,plb,pl;


    for (i=0;i<nmus;i++){
      pla = 1.;
      plb = mus(i);

      xi(i) = pla * cl(0) + 3.*plb*cl(1);
    
      for (l=2; l < lmax; l++){
        dl=(double) l;

        pl = ((2.*dl-1.)*mus(i)*plb-(dl-1.)*pla)/dl;
        pla = plb;
        plb = pl;

        xi(i) += (2*dl +1) * pl * cl(l);
      }
    }
    
    """
    scipy.weave.inline(code,['xi','lmax','mus','nmus','cl'],
                 type_converters = cblitz,
                 compiler='gcc')       

    return xi/4./N.pi


def legP_3d(kbands,mu,weight,lmax=1000):
    """
      calculate a vector containing
      p_0(amu),p_1(amu),...,p_lmax-1(amu)
      where p_l is a Legendre polynomial, and amu is a cos angle
    """
    
    #print type(lmax)
    #print type(amu)
    
    #print lmax,amu
    #assert(type(amu) == type(1.0))
    #assert(type(amu) == type(N.array([1.0])[0]) or type(1.0))

    try:
        assert(weight.shape[1] == len(kbands)-1)
    except:
        print "weight dim prob:",weight.shape,len(kbands)
        raise
    assert(weight.shape[0] == lmax)

    #print "clib: lmax",lmax

    s = mu.shape
    
    pl = scipy.zeros(lmax,'d')
    dmat = scipy.zeros((len(kbands)-1, s[0], s[1]),'d')
    code =\
    """                                                                   
    int l,i,j,ki,b,foundbin;
    double dl,tmp;

    // printf(">>mu shape %i %i\\n",Nmu[0],Nmu[1]);
    // loop over 2-dim array mu
    for (i=0; i < Nmu[0]; i++){
      printf("%i/%i\\n",i,Nmu[0]);
      for (j=i; j < Nmu[1]; j++){
        // compute pl at this mu
        pl(0)=1.;
        pl(1)=mu(i,j);
        for (l=2; l < lmax; l++){
          dl=(double) l;
          pl(l)=((2*dl-1)*mu(i,j)*pl(l-1)-(dl-1)*pl(l-2))/dl;
        }

        for (b=0; b<Nkbands[0]-1; b+=1){
          tmp = 0;
          for (l=0; l<lmax; l++){
            tmp += pl(l)*(2*l+1)*weight(l,b);
          }
          dmat(b,i,j) += tmp;
          if (i!=j){dmat(b,j,i) += tmp;}
        }
        
     } // end j loop
    }  // end i loop
    """ 
    scipy.weave.inline(code,['dmat','kbands','pl','lmax','mu','weight'],
                 type_converters = cblitz,
                 compiler='gcc')
    return dmat/(4*N.pi)






def legP_3d_parallel(kbands,mu,weight,lmax=1000):
    """
      calculate a vector containing
      p_0(amu),p_1(amu),...,p_lmax-1(amu)
      where p_l is a Legendre polynomial, and amu is a cos angle
    """
    
    #print type(lmax)
    #print type(amu)
    
    #print lmax,amu
    #assert(type(amu) == type(1.0))
    #assert(type(amu) == type(N.array([1.0])[0]) or type(1.0))

    try:
        assert(weight.shape[1] == len(kbands)-1)
    except:
        print "weight dim prob:",weight.shape,len(kbands)
        raise
    assert(weight.shape[0] == lmax)

    #print "clib: lmax",lmax

    s = mu.shape
    
    #pl = scipy.zeros(lmax,'d')
    dmat = scipy.zeros((len(kbands)-1, s[0], s[1]),'d')
    code =\
    """
    int l,i,j,ki,b,foundbin,status;
    int tid,nthreads;
    double dl,tmp,pl[10000];

    status = 0;
    nthreads = 0;

    // printf(">>mu shape %i %i\\n",Nmu[0],Nmu[1]);
    // loop over 2-dim array mu
    #pragma omp parallel for private(i,j,pl,l,ki,b,foundbin,dl,tmp,tid)
    for (i=0; i < Nmu[0]; i++){
      tid = omp_get_thread_num();
      if (tid == 0) {nthreads = omp_get_num_threads();}
      
      status += 1;
      fprintf(stderr,"\\rComputing deriv matrix: %i/%i (thread %i of %i)",status,Nmu[0],tid,nthreads);
      for (j=i; j < Nmu[1]; j++){
        // compute pl at this mu
        pl[0]=1.;
        pl[1]=mu(i,j);
        for (l=2; l < lmax; l++){
          dl=(double) l;
          pl[l]=((2*dl-1)*mu(i,j)*pl[l-1]-(dl-1)*pl[l-2])/dl;
        }

        for (b=0; b<Nkbands[0]-1; b+=1){
          tmp = 0;
          for (l=0; l<lmax; l++){
            tmp += pl[l]*(2*l+1)*weight(l,b);
          }
          dmat(b,i,j) += tmp;
          if (i!=j){dmat(b,j,i) += tmp;}
        }
        
     } // end j loop
    }  // end i loop
    """ 
    scipy.weave.inline(code,['dmat','kbands','lmax','mu','weight'],
                       extra_compile_args =['-O3 -fopenmp'],
                       extra_link_args=['-lgomp'],
                       headers=['<omp.h>'],
                       type_converters = cblitz,
                       compiler='gcc')
    return dmat/(4*N.pi)
