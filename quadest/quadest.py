import sys,os
sys.path.append("..")
sys.path.append(".")
import pylab
import healpy
import numpy as N

import theorycl
import cPickle as pickle
import clib
import scipy
from scipy.interpolate import interp1d
import bins
import time
import inv
import dmat
import fisher as gofish
import healpixtools
import covmat


def go(map, mask, map2=None, mask2=None,  noise=None, 
        fidcl=None, bands=None, fishbands=None, label='ciao', bignum=100., 
        map_order='nested',mode='lcl',
        cachedir='cache'):
    """ 
    quadratic estimator

    Inputs
    -------


    Outputs
    -------

    """
    if map_order.lower()[0]=='n':
        nest=True
    else:
        nest=False
    print "> map ordering: nested =",nest
        
    nside = healpy.npix2nside(len(map))

    # multiply input Cl's by pixel window function
    Bl = healpixtools.pixwin(nside, len(fidcl))
    cl = fidcl*Bl**2
    l = N.arange(len(fidcl))

    # insert big numbers into monopole and dipole so they are projected out
    cl[0] = bignum
    cl[1] = bignum

    # collapse data vector according to mask
    maskii = N.where(mask>0)
    data = map[maskii]

    npix = N.sum(mask)
    print "npix",npix

    # compute covariance matrix
    covMatInv = covmat.covmat(cl, mask, label=label, noise = noise,
                                cacheinv='cache/cmatinv_%s.npy')

    # compute weights to arrive at l*Cl corrected for the pixel window
    if mode=='lcl':
        ll = N.arange(len(Bl))
        ll[0]=1
        weight = Bl**2/ll
    elif mode=='cl':
        weight = Bl**2
    else:
        print "unknown mode!",mode
        exit(1)


    # horribly fat fisher matrix
    if fishbands is None:
        fishbands = bands
    fishlabel = '%i_%s'%(len(covMatInv),label)
    if not os.path.exists('%s/fisher'%cachedir):
        os.mkdir('%s/fisher'%cachedir)
    fishoutput = "%s/fisher/fish_%s.npy"%(cachedir,fishlabel)

    fish = gofish.fisher(covMatInv, fishbands, mask, weight=weight,
                         tag=fishlabel, output=fishoutput)

    cmx = N.dot(covMatInv,data)
    CinvN = N.dot(covMatInv, N.eye(len(covMatInv))*noise[maskii])

    assert(N.abs(cmx.max()/cmx.min()) < 1e10)
    assert(N.all(N.isfinite(cmx)))

    # compute derivative matrices
    print "> now derivative matrices"
    dMats = dmat.dmat_fast(bands,mask,weight=weight,cache=None)

    print "> computing estimate"

    assert(N.all(N.isfinite(dMats[0])))
    a = N.dot(dMats[0],cmx)
    b = N.dot(cmx,a)
    assert(N.all(N.isfinite(a)))
    assert(N.isfinite(b))

    est = 0.5*N.array([N.dot(cmx,N.dot(dm,cmx)) for dm in dMats])
    print "> done"

    assert(N.all(N.isfinite(est)))


    print "> computing Cinv dot dmat"
    for i in range(len(dMats)):
        # overwrite dmat
        dMats[i] = N.dot(covMatInv,dMats[i])
    cInvDm = dMats
    print "\ndone"


    print "> computing noise bias"
    noiseb = []
    for l1 in range(len(cInvDm)):
        noiseb.append((cInvDm[l1]*CinvN.transpose()).sum())
    noiseb = 0.5*N.array(noiseb)

    print "> done"

    clb = bins.binit(l,l*fidcl,fishbands)
    bx = (bands[1:]+bands[:-1])/2.

    results = bx,clb,est,noiseb,bands,fishbands,fish,fidcl
    return results






if __name__ == "__main__":
    pass
