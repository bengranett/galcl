import numpy as N
import pylab

counter = 0
def smartpinv(m, rcond=1e-15):
    """ """
    global counter
    u,s,vt = N.linalg.svd(m)
    v = N.abs(s)/N.abs(s).max()
    pylab.figure()
    pylab.semilogy(v)
    pylab.semilogy(v[N.where(v>rcond)])
    pylab.axhline(rcond)
    pylab.savefig('pinv_%i.pdf'%counter)
    pylab.close()
    counter += 1
    return N.linalg.pinv(m, rcond=rcond)


def pinv(m, cut=0, docheck=False):
    """ Compute a pseudo-inverse using the singular-value
    decomposition (SVD).  Can specify a number of the smallest (in
    magnitude) singular values to set to 0."""

    if cut == 0:
        return N.linalg.inv(m)

    print "doint pseudo inv with cut=",cut
    u,s,vt = N.linalg.svd(m)

    if docheck:
        check = N.dot(u,N.dot(N.diag(s),vt))
        assert(N.allclose(m,check))
        print "check passed"

    sinv = 1./s.copy()
    order = N.argsort(N.abs(s))

    if cut < 0:
        ii = order[cut:]
    else:
        ii = order[:cut]

    print 'modes set to 0:',len(ii),cut

    sinv[ii] = 0

    ut = u.transpose()
    v = vt.transpose()
    ss = N.diag(sinv)

    out = N.dot(v, N.dot(ss, ut))
    return out

def test():
    m = N.random.normal(0,1,(4,4))
    a = pinv(m, cut=0, docheck=True)

    print N.dot(a,m)


if __name__=="__main__":
    test()
