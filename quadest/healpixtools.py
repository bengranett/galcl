import numpy as N
import healpy


def pixwin(nside, lmax):
    """ Get healpix window function and extrapolate to lmax

    Inputs
    ------
    nside
    lmax

    Ouputs
    -------
    window 
    """
    wf = healpy.pixwin(nside)
    i = len(wf) - 1
    # match the slope
    m = N.log(wf[i]) - N.log(wf[i-1])
    sigsq = -i/m
    x = N.arange(i+1, lmax)
    y = N.exp(-x**2/(2*sigsq))
    n = wf[i]/N.exp(-i**2/(2*sigsq))
    y *= n
    w = N.concatenate([wf, y])
    return w

def compute_separations(mask, nest=True, eps=1e-10):
    """ """
    nside = healpy.npix2nside(len(mask))
    ii = N.where(mask==1)
    s = N.arange(12*nside**2)[ii]
    v = healpy.pix2vec(nside, s, nest=nest)

    costhij = N.multiply.outer(v[0],v[0])+ \
              N.multiply.outer(v[1],v[1])+N.multiply.outer(v[2],v[2])

    print "> mu min max",costhij.min(),costhij.max()
    costhij[N.where(costhij>1-eps)]=1-eps
    costhij[N.where(costhij<-1+eps)]=-1+eps

    thetamax = N.arccos(costhij.min())*180/N.pi

    return costhij, thetamax


def gridpoints(data, nside=512, nest=True):
    """ """
    ra,dec = N.transpose(data)
    lon = N.pi/180*ra
    lat = N.pi/180*(90-dec)

    i = healpy.ang2pix(nside, lat, lon, nest=nest)
    map = N.zeros(12*nside**2)
    for a in i:
        map[a] += 1

    return map
