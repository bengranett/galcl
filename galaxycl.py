import sys
from sample import Sample
import numpy as N
import ISWCalc
import params

sys.path.append("quadest")
import quadest

import cPickle as pickle

class GalaxyCl:
    """ Compute the angular power spectrum"""
    
    params = {
        'template_ell_max': 10000,
        'ell_max': 1200,
        'ell_min': 0,
        'ell_step': 25,

        'fish_ell_min': 0,
        'fish_ell_max': 1200,
        'fish_ell_step': 25,

        'randoms_ntimes':10,

        'map_nside':512,
        'map_order':'nested',
        }

    cosmoparams = {
        'hubble': 70.,
        'omega_cdm': 0.300-0.045,
        'omega_baryon': 0.045,
        'omega_lambda': 0.7,
        'do_nonlinear': 1,
        'scalar_spectral_index':[0.96],
    }

    samples = {}
    sample_list = []

    cl_model = None

    def __init__(self, sample1, sample2=None,
                 **params):
        """ """
        self.add_sample(sample1)
        if sample2 is not None:
            self.add_sample(sample2)
        for k in params:
            if self.params.has_key(k):
                self.params[k] = params[k]
            elif self.cosmoparams.has_key(k):
                self.cosmoparams[k] = params[k]
            else:
                print "! Warning! Unknown parameter:",k,params[k]

        self.init_params()


    def init_params(self):
        """ """
        h = self.cosmoparams['hubble']/100.
        self.cosmoparams['ombh2'] = self.cosmoparams['omega_baryon']*h**2
        self.cosmoparams['omch2'] = self.cosmoparams['omega_cdm']*h**2


    def add_sample(self, sample):
        """ Add a galaxy sample to the estimator.

        Inputs
        ------

        Outputs
        -------
        None

        """
        if self.samples.has_key(sample.name):
            print "> Error! Sample %s is already defined"%sample.name
            return
        self.samples[sample.name] = sample
        self.sample_list.append(sample.name)
        print "> Added sample %s"%sample.name
        
    def modelCl(self):
        """ 
        Compute Cl model
        Inputs
        ------

        Outputs
        ------

        """
        bias = []
        zdist = []
        zz = []
        tag = ""
        for k in self.samples:
            z,dndz = self.samples[k].zdist
            zz.append(z)
            zdist.append(dndz)
            bias.append( self.samples[k].bias )
            tag += k

        dndz1 = z,zdist[0]
        dndz2 = None
        if len(zdist)>1:
            dndz2 = z,zdist[1]

        # galaxy bias
        if len(bias)==1:
            bg2 = bias[0]**2
        else:
            bg2 = bias[0]*bias[1]


        if len(zdist)==1:
            redshift = N.sum(zdist[0]*z)/N.sum(zdist[0])
        else:
            redshift = N.sum(zdist[0]*zdist[1]*z)/N.sum(zdist[0]*zdist[1])


        C = ISWCalc.ISWCalculator(dndz1, cambz=redshift,
                                  params=self.cosmoparams)
        
        l = N.arange(0,self.params['template_ell_max'])
        print "> compute Cl"
        cl = C.GalaxyCl_flat(l, dndz2=dndz2)

        cl *= bg2

        print "> bias square",bg2


        self.cl_model = cl

        return l,cl

    def make_maps(self):
        """ """
        print "> make_maps"
        for name in self.sample_list:
            S = self.samples[name]
            S.random_sample(ntimes=self.params['randoms_ntimes'])
            S.make_healpix_map(nside=self.params['map_nside'])

    def quadest(self, sample_name=None):
        """ """
        if self.cl_model is None:
            self.modelCl()

        if sample_name==None: 
            sample_name=self.sample_list[0]

        S = self.samples[sample_name]
        if S.delta_map is None:
            self.make_maps()

        bands = N.arange(self.params['ell_min'],self.params['ell_max'],self.params['ell_step'])
        fishbands =  N.arange(self.params['fish_ell_min'],self.params['fish_ell_max'],self.params['fish_ell_step'])
        
        results = quadest.go(S.delta_map, S.mask_map, fidcl=self.cl_model, bands=bands, noise=S.var_map, fishbands=fishbands, 
                             label=sample_name, map_order=self.params['map_order'])

        pickle.dump(results, file("results/results_%s.pickle"%sample_name,'w'))

        return results
